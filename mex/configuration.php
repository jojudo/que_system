<?php 
//bring down the site
$config['offline'] = 0;
$config['offline_msg'] = 'Im sorry site is under construction';
$config['session_timeout'] = 86400; // will be leaving session on for 24 hours
$config['strike_limit'] = 5; //5 wrong password
$config['strike_wait'] = 300; //5 minutes wait for fresh 5 strike bat

$config['timezone'] = 'Pacific/Samoa';
$config['environment'] = 'development';
$config['theme'] = 'queads';

//database settings
$config['db']['host'] = 'localhost';
$config['db']['database'] = 'multi_que';
$config['db']['driver'] = 'mysql';
$config['db']['username'] = 'root';
$config['db']['password'] = '';

//li braries
//for javascripts
$config['lib']['js'] = array('jquery/jquery-1.10.2.js','jquery/jquery-ui.min.js','framework/main.js');

//for css
$config['lib']['css'] = array('jquery/jquery-ui.min.css');

//extras
$config['data']['limit'] = 20;

//in megabytes = 10MB
$config['upload']['limit'] = 60971520;
$config['upload']['dir'] = 'upload';

/*--------------- YOU CAN ADD CUSTOM SETTINGS FROM HERE ------------------*/
//AD PAGE SETTINGS
//number of seconds to wait before checking if a there is a new number
$config['monitor']['buzzerTimerSeconds'] = 1; 

//number of seconds to wait before checking if a there is new marquee Data
$config['monitor']['marqueeTimerSeconds'] = 10; 

//number of seconds to wait before checking if a there is new AD Data
$config['monitor']['adTimerSeconds'] = 1800; 

//video player dimensions
$config['monitor']['videoPlayerWidth'] = 800; 
$config['monitor']['videoPlayerHeight'] = 600; 

//IMage dimensions
$config['monitor']['imageWidth'] = 800; 
$config['monitor']['imageHeight'] = 500;


?>