<?php
	defined('INSTANCE') or die("No Direct access allowed!");
	class ui{
		protected $stylesheet, $javascript, $datastore, $headerType;
		function __construct(){
			$this->headerType = statics::request("headerType");
			$this->datastore = load_class('datastore');
			$this->load_libraries();
		}
		
		function set_module($mod){
			$this->mod = $mod;
		}
		
		function ui_start(){
			ob_start();
		}	
		
		function load_theme($body){
			if($this->headerType=="none"){
				print $body;
			}else{
				$headinclude= $this->get_theme_head();
				$topnav = $this->get_theme_navigation();
				include_once(THEME_BASE."index.tpl");				
			}
			return ;
		}
		
		function ui_end($outputFlag=TRUE){
			if($outputFlag)	$this->load_theme(ob_get_clean());	else return ob_get_clean();
		}
		
		function set_stylesheet($link){
			if($this->datastore->get_config('environment')=="development")
				$link.='?temp='.rand(1,9999999);
				
			$this->stylesheet[] = $link;
		}
		
		function set_javascript($link){
			if($this->datastore->get_config('environment')=="development")
				$link.='?temp='.rand(1,9999999);
				
			$this->javascript[] = $link;
		}
		
		function link_name(){
			$args = func_get_args();
			
			$link =  FULL_ADDR.'index.php?mod='.$args[0];
			if(isset($args[1]) && $args[1]) $link.='&action='.$args[1];
			
			$totalArgs = count($args);
			if($totalArgs > 2){
				for($i=2;$i< $totalArgs;$i++){
					$link .= '&var'.($i-1).'='.$args[$i];
				}
			}
				
			//param1 = mod param 2 = action
			
			
			
			return $link;
		}
		
		function get_theme_navigation(){
			$args = array();
			if(isset($this->mod))
				return $this->load_theme_page('navigation',$args);
			else
				return '';
		}
		
		
		
		function load_theme_page_solo($filename,$args){
			$tpl = $filename.'.tpl';
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				$headinclude= $this->get_theme_head();
				include_once(THEME_BASE.$tpl);
			}else{
				$this->load_theme_errorfile('Page was not found');
			}
			
			return ;
		}
		
		function load_theme_page_noheader($filename,$args){
			$tpl = $filename.'.tpl';
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				include_once(THEME_BASE.$tpl);
			}else{
				$this->load_theme_errorfile('Page was not found');
			}
		}
		
		function load_theme_errorfile($msg){
			$args['msg'] = $msg;
			print $buffer = $this->load_theme_page('errorfile',$args);
		}		
		
		function load_theme_page($filename,$args){
			$tpl = $filename.'.tpl';
			ob_start();
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				include_once(THEME_BASE.$tpl);
			}else{
				print 'Theme file '.THEME_BASE.$tpl.' is missing ';
			}
			$main_body = ob_get_clean();
			
			return $main_body;
		}
		
		function use_module_flat($filename,$args=array()){
			$tpl = $filename.'.tpl';
			$args['module_addr'] = MOD_ADDR.$this->mod.US;
			if(file_exists(MODPATH.$this->mod.DS.'flats'.DS.$tpl)){
				extract($args);
				include(MODPATH.$this->mod.DS.'flats'.DS.$tpl);
			}else{
				print 'Unable to find template '.$tpl;
			}
		}
		
		function load_libraries(){		
			$lib_arr = $this->datastore->get_config('lib');
			if(is_array($lib_arr)){
				foreach($lib_arr['js'] as $js){
					$this->set_javascript(LIB_ADDR.'js'.US.$js);
				}
				
				foreach($lib_arr['css'] as $css){
					$this->set_stylesheet(LIB_ADDR.'css'.US.$css);
				}
			}
		}
		
		function realtime_css_library($folder,$file=''){
			$file = $file?$file:$folder;
			$this->set_stylesheet(LIB_ADDR.'css'.US.$folder.US.$file.'.css');
		}
		
		function realtime_js_library($folder,$file=''){
			$file = $file?$file:$folder;
			$this->set_javascript(LIB_ADDR.'js'.US.$folder.US.$file.'.js');
		}

		
		function set_js_variables($jsVar=array()){
			foreach($jsVar as $index=>$val){
				$this->jsVar[$index] = $val;
			}
		}
		
		//Manage Tempate Headers
		function get_theme_head(){
			return $this->template_headers_auto_setup();
		}
		
		function template_headers_auto_setup(){
			$spacer = "\r\n";
			$header_str = $spacer;
			if(isset($this->jsVar) && count($this->jsVar)){
				
				$header_str .= '<script language="javascript">'.$spacer;
				foreach($this->jsVar as $varName=>$varVal){
					$header_str.= "var $varName = '$varVal';".$spacer;
				}
				$header_str.= '</script>'.$spacer;
			}
			
			//javascripts
			if(isset($this->javascript) && is_array($this->javascript)){
				foreach($this->javascript as $js){
					$header_str.='<script src="'.$js.'" language="javascript"></script>'.$spacer;
				}
			}
			//stylesheets
			if(isset($this->stylesheet) && is_array($this->stylesheet)){
				foreach($this->stylesheet as $css){					
					$header_str.='<link rel="stylesheet" type="text/css" media="screen" href="'.$css.'" />'.$spacer;
				}
			}
			
			return $header_str;
		}
		
		function get_header_type(){
			return $this->headerType;
		}
		
	}
?>