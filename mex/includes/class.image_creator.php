<?php
	//image resizing
	class image_creator
	{
		var $file_name;
		var $folder_location;
		var $accepted_types = array('gif','png','jpeg','jpg');
		var $maxW;
		var $maxH;
		
		function __construct($image,$folder,$width,$height)
		{
			$this->file_name = $image;
			$this->folder_location = $folder;
		    $this->maxW = $width;
		    $this->maxH = $height;
		}
		
		function return_ext() 
		{
			$file_types = $this->accepted_arrays;
			
			$img = $this->file_name;
			$img_arr = explode(".",$img);
			$ctr = count($img_arr);
			$ext = strtolower($img_arr[$ctr-1]);
			
		    return $ext;
		}
		
		
		function process_for_output()
		{
			$limitW = $this->maxW;
			$limitH = $this->maxH;
			$img = $this->folder_location.$this->file_name;
			$ext = $this->return_ext();
			if(!file_exists($img) || $this->file_name=="")
			{
				$img = "images/bg_default.png";
				$ext="png";
			}
			
			
			if(in_array($ext,$this->accepted_types) && file_exists($img))
			{
				if($ext=="gif")
					$im = imagecreatefromgif($img);
				elseif ($ext=="png")
					$im = imagecreatefrompng($img);
				else
					$im = imagecreatefromjpeg($img);
			
				
				list($width, $height) = getimagesize($img);
				$xscale=$width/$limitW;
		    	$yscale=$height/$limitH;
		    
				//$newwidth = $width * $percent;
				//$newheight = $height * $percent;
				
				if ($yscale>$xscale){
			        $new_width = round($width * (1/$yscale));
			        $new_height = round($height * (1/$yscale));
			    }
			    else {
			        $new_width = round($width * (1/$xscale));
			        $new_height = round($height * (1/$xscale));
			    }
				
				// Load
				$thumb = imagecreatetruecolor($new_width, $new_height);
				
				// Resize
				imagecopyresampled($thumb, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);			
					
				
			}
			else
			{
			  	$thumb= imagecreatetruecolor ($limitW, $limitH);
		        $bgc = imagecolorallocate ($thumb, 255, 255, 255);
		        $tc = imagecolorallocate ($thumb, 0, 0, 0);
		
		        imagefilledrectangle ($thumb, 0, 0, $limitW, $limitH, $bgc);
		
		        /* Output an error message */
		        imagestring ($thumb, 3, 5, 30, 'Image Unavailable', $tc);
			}
			
			return $thumb;
			
		}
		
		function txttoImage($bg,$text){
			
			$imbg = imagecreatefrompng("images/push_pin.png");
			imagealphablending($imbg, false);
			imagesavealpha($imbg, true);
			
			$imTxt = imagecreatetruecolor(50, 40);
			$red = imagecolorallocate($imTxt, 255, 200, 115);
			$black = imagecolorallocate($imTxt, 0x00, 0x00, 0x00);
			
			// Make the background red
			imagefilledrectangle($imTxt, 0, 0, 299, 99, $red);
			
			// Path to our ttf font file
			$font_file = MEX_BASE.DS.'images'.DS.'arial.ttf';
			
			// Draw the text 'PHP Manual' using font size 13
			$size = 10;
			if(strlen($text)>=5)
				$size=8;
				
			imagefttext($imTxt, $size, 0, 5, 25, $black, $font_file, $text);
			
			// Output image to the browser
			header('Content-Type: image/png');
			
			imagecopymerge($imbg,$imTxt,8,15,5,16,35,10,80);
			
			imagepng($imbg);
			imagedestroy($imbg);
			imagedestroy($imTxt);
		}
	}
?>