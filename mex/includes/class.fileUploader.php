<?php
	//load required external library
	load_lib('qqUploadedFileXhr');
	
	class fileUploader{
		protected $uploader, $settings, $result;
		
		function __construct($initiate = TRUE){
			$allowedExtensions = array(	);
			$datastore = load_class('datastore');
			$this->settings =  $datastore->get_config('upload');
			
			//set up max uploading in the ini_get
			ini_set('upload_max_filesize',($this->settings['limit']/ 1024 / 1024).'M');
			ini_set('post_max_size',($this->settings['limit']/ 1024 / 1024).'M');
			
			$this->uploader = new qqFileUploader($allowedExtensions,$this->settings['limit']);
			if($initiate){
				return $this->begin_upload();
			}
			
		}
		
		function begin_upload(){
		
			$ui = load_class("ui");
			if(!is_dir(BASE.DS.$this->settings['dir'].DS.$ui->mod)){
				//create folder
				if(is_writable(BASE.DS.$this->settings['dir'])){
					mkdir(BASE.DS.$this->settings['dir'].DS.$ui->mod);
				}else{
					return array('success'=>0,'error'=>'Upload folder is not writeable');
				}
				
			}
			
			$finalFolder = BASE.DS.$this->settings['dir'].DS.$ui->mod.DS;				
			$this->result = $this->uploader->handleUpload($finalFolder);
			
		}
		
		function move_file($newFile){
			$ui = load_class("ui");
			$currentFile = BASE.DS.$this->settings['dir'].DS.$ui->mod.DS.DS.$this->result['filename'].'.'.$this->result['ext'];				
			$newFile = BASE.DS.$newFile;
			return rename($currentFile, $newFile);
		}
		
		function get_status(){
			return $this->result;
		}
		
	}
 ?>