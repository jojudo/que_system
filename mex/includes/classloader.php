<?php

if ( ! function_exists('load_class'))
	{
		function &load_class($class)
		{
			static $_classes = array();
			$prefix = 'class';
			
			// Does the class exist?  If so, we're done...
			if (isset($_classes[$class]))
			{
				return $_classes[$class];
			}

			$name = FALSE;

			
			if (file_exists(CLASSPATH.DS.$prefix.'.'.$class.'.php'))
			{
				$name = $class;

				if (class_exists($name) === FALSE)
				{
					require(CLASSPATH.DS.$prefix.'.'.$class.'.php');
				}
			}else{
					exit("it does not exist");
			}
		

			// Did we find the class?
			if ($name === FALSE)
			{
				// Note: We use exit() rather then show_error() in order to avoid a
				// self-referencing loop with the Excptions class
				exit('Unable to locate the specified class: '.$class.'.php');
			}

			// Keep track of what we just loaded
			is_loaded($class);

			$_classes[$class] = new $name();
			return $_classes[$class];
		}
	}
	if ( ! function_exists('load_lib'))
	{	
		function &load_lib($folder,$filename='',$classname='',$instantiate=FALSE){
			//folder ex: 'folder/subfolder' 
			$filename = $filename?$filename:$folder;
			$classname = $classname?$classname:$filename;
			
			static $_classes = array();
			
			// Does the library exist?  If so, we're done...
			//we separate the library classes from the core classes
			if (isset($_classes[$classname]))
			{
				return $_classes[$classname];
			}

			$name = FALSE;
			$folder = LIBPATH.DS.$folder;
			
			if (file_exists($folder.DS.$filename.'.php'))
			{
				$name = $classname;

				if (class_exists($name) === FALSE)
				{
					require($folder.DS.$classname.'.php');
				}
			}
		

			// Did we find the class?
			if ($name === FALSE)
			{
				// Note: We use exit() rather then show_error() in order to avoid a
				// self-referencing loop with the Excptions class
				exit('Unable to locate the library class '.$class.' in '.$folder);
			}

			// Keep track of what we just loaded
			if($instantiate===TRUE){
				is_loaded($classname);

				$_classes[$classname] = new $name();
				return $_classes[$classname];
			}else{
				return $instantiate;
			}
			
		}
	}
	
	if ( ! function_exists('is_loaded'))
	{
		function &is_loaded($class = '')
		{
			static $_is_loaded = array();

			if ($class != '')
			{
				$_is_loaded[strtolower($class)] = $class;
			}

			return $_is_loaded;
		}
	}