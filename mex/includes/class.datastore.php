<?php 
	class datastore{ 
		protected $parameter_basket;
		protected $profile = array();
		function __construct(){
			//fill up the basket of variables
			$this->parameter_picking();
		}
		
		function set_config($config_arr){
			$this->config = $config_arr;
		}
		
		function set_profile($profile_arr){
			$this->profile = $profile_arr;
		}
		
		function get_config($subgroup=""){
			if(!$subgroup)
				return array();
			else
				return $this->config[$subgroup];
		}
		
		function get_profile(){
			return $this->profile;
		}
		
		function parameter_picking(){
			
			//set and filter possible parameter sources
			if(isset($_GET)){
				$this->parameter_basket['GET'] = $this->parameter_cleaning($_GET);  
			}
			if(isset($_POST)){
				$this->parameter_basket['POST'] = $this->parameter_cleaning($_POST);  
			}
			
			$this->parameter_basket['ALL'] = array_merge($this->parameter_basket['POST'],$this->parameter_basket['GET']);
				
		}
		
		function parameter_cleaning($var_arr){
			$return_val[] = array();
			foreach($var_arr as $index=>$value){	
				if(is_array($value) && count($value))
					$return_val[$index] = $this->parameter_cleaning($value);
				else
					$return_val[$index] = $this->sanitizer($value);
					
			}
			return $return_val;
		}
		
		function pick($paramName){
			if(!isset($this->parameter_source))
				$this->parameter_source = 'GET';
			 
			return isset($this->parameter_basket[$this->parameter_source][$paramName])?$this->parameter_basket[$this->parameter_source][$paramName]:FALSE;	
		}
		
		function set_parameter_source($source){
			if(in_array($source,array('GET','POST','ALL','REWRITE')))
				$this->parameter_source = $source;
			else
				$this->parameter_source = 'GET';
		}
		
		function get_basket($type='GET'){
			return $this->parameter_basket[$type];
		}
		
		function sanitizer($val){
	 		$var= $val;
	 		$var = trim($var);
	 		$var = strip_tags($var); 		
	 		$var = mysql_escape_string($var);
	 		return $var;
	 	}
	}
