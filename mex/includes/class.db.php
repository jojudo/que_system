<?php
class db{

	protected $host, $username, $password, $database, $rs, $db, $sql, $conn, $error;
	
	function __construct(){
		$datastore = load_class('datastore');
		$dbconfig = $datastore->get_config('db');
		
		$this->host = $dbconfig['host'];
		$this->username = $dbconfig['username'];
		$this->password = $dbconfig['password'];
		$this->database = $dbconfig['database'];		
		$this->driver = $dbconfig['driver'];		
		
		$this->connect_db();
	}
	
	function connect_db(){
		try{
			$this->conn = new PDO($this->driver.':host='.$this->host.';dbname='.$this->database.';charset=utf8', $this->username, $this->password);		
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			
		}catch(PDOException $ex){
			echo $this->error = $ex->getMessage();
			exit();
		}
	}
	
	
	//general mysql query call
	function queryMe(){	
		try {
			//connect as appropriate as above
			$this->rs = $this->conn->query($this->sql); //invalid query!
			return $this->rs;
			
		} catch(PDOException $ex) {
			echo $this->error = $ex->getMessage().$this->sql;
			exit();
			return false;
		}		
	
	}
	
	//transactional mysql query call
	function transactionalQuery($action){
		try {  
			$this->conn->beginTransaction();
			$this->rowCount = $this->conn->exec($this->sql);
			if($action=="insert")
				$this->latestInsertedId = $this->conn->lastInsertId();
			$this->conn->commit();
			
			return true;
			
		} catch (Exception $e) {
			$this->conn->rollBack();
			$this->error = $e->getMessage();
			return false;
			
		}	
	}
	//build a select query
	function buildSelectSql(){
		//parameters: $table,$fields,$condition="",$order="",$limit="",$group=""
		list($table,$fields,$condition,$order,$limit,$group) = func_get_args();
		$this->sql = "select $fields from $table";
		if($condition){
			//place all conditions
			$this->sql.=" where ";
			if(is_array($condition)){
				//enter here if the conditions where passed on as an array
				if(count($condition['and'])){
					$ctr=0;
					foreach($condition as $items){
						$ctr++;
						if($ctr>1) $this->sql.=' and ';
						$this->sql.=$items;
					}
				}
				
				if(count($condition['or'])){
					$ctr=0;
					foreach($condition as $items){
						$ctr++;
						if($ctr>1) $this->sql.=' or ';
						$this->sql.=$items;
					}
				}
			}else{
				//if condition is a string
				$this->sql.=" $condition";
			}
			
		}
		if($group) $this->sql.=" group by $group";
		if($order) $this->sql.=" order by $order";
		if($limit) $this->sql.= " limit $limit";
		
	}
	
	//do a select query
	function selectQuery(){
		//parameters: $table,$fields,$condition="",$order="",$limit="",$group=""
		list($table,$fields,$condition,$order,$limit,$group) = func_get_args();
		$this->buildSelectSql($table,$fields,$condition,$order,$limit,$group);
		$rs = $this->queryMe();
		if(!$rs){
			echo $this->gerErrorMsg();
			exit();
		}
		return $rs;
	}
	
	function selectQueryArr($table,$fields,$condition="",$order="",$limit="",$group=""){
		//list() = func_get_args();
		$this->buildSelectSql($table,$fields,$condition,$order,$limit,$group);
		$rs = $this->queryMe();
		$this->rowCount  = $rs->rowCount();
		
		return $rs->fetchAll(PDO::FETCH_ASSOC);
	}	
	
	function selectQueryFirst($table,$fields,$condition="",$order="",$limit="",$group=""){
		//list() = func_get_args();
		$this->buildSelectSql($table,$fields,$condition,$order,$limit,$group);
		$rs = $this->queryMe();
		return $rs->fetch(PDO::FETCH_ASSOC);
	}
	
	//insert queries
	function insertQuery($table,$data){
		//string, array
		if(is_array($data) && count($data)){
			foreach($data as $field=>$value){
				$fields_arr[] = $field;
				$bounds_arr[] = ':'.$field;
				//$values_arr[':'.$field] = $value;
				$values_arr[] = $value;
			}
			$fields = implode(",",$fields_arr);
			$values = implode("','",$values_arr);
			$this->sql = "INSERT INTO $table ($fields) VALUES ('$values')";
			return $this->transactionalQuery("insert");
		}
	}
	
	function getInsertId(){
		return $this->latestInsertedId;
	}
	
	//delete query
	function deleteQuery($table,$condition){
		$this->sql = "DELETE FROM $table WHERE $condition";
		//$this->queryMe();
		return  $this->transactionalQuery("delete");
	}
	
	function updateQuery($table,$fieldvalue,$condition){
		$sql = "update $table set ";	
		$ctr=0;
		foreach($fieldvalue as $field=>$value){
			$ctr++;
			if($ctr!=1){
				$sql.=",";
			}
			$sql .="$field='$value' ";
		
		}
		$sql .="where $condition";
		
		$this->sql = $sql;
		$this->transactionalQuery('update');
		
		return $this->rowCount;
	}
	
	function num_rows(){
		return $this->rowCount;
	}
	
	function found_rows(){
		$this->sql = "select FOUND_ROWS() as found";
		$this->queryMe();
		$arr = $this->rs->fetch(PDO::FETCH_ASSOC);
		
		return $arr['found'];
		
	}
	
	function freeMe($rs)
 	{
 		mysql_free_result($rs);
 	}
 	
 	function disconnect_db()
 	{
 		mysql_close($this->conn);
 	}
	
	function show_sql(){
		echo $this->sql;
	}
	
	function getErrorMsg(){
		return $this->error;
	}

	//-------------------------------- new query functions --------------------//

	
	public function thisFetchAssoc(){
		return mysql_fetch_assoc($this->rs);
	}

}

?>