<?php
	//load the parent to class repository without initializing it
	load_lib('phplot','phplot','PHPlot');
	
	class plotter extends phplot{
			private $dataset = array(), $labelset = array() , $plotsettings=array();
			
			function __construct(){
				//create defaults
				$this->set_dimensions();
			}
			
			function set_dimensions($width=300,$height=200,$outputfile=null,$inputfile=null){
				$this->plotsettings = array($width,$height,$outputfile,$inputfile);
			}
			
			function initiate_graphing(){
				$this->initialize('imagecreate', $this->plotsettings[0], $this->plotsettings[1], $this->plotsettings[2], $this->plotsettings[3]);
			}
			
			function set_labels($data){
				$this->labelSet = $data;
			}
			
			function set_data($data){
				$this->dataSet = $data;
			}
			
			function make_bar_chart($title){
				$this->SetDataValues($this->dataSet);
				$this->SetImageBorderType('plain');
				$this->SetPlotType('bars');
				$this->SetTitle($title);
					
				
				$this->output_chart();
			}
			function make_pie_chart($title){
				$this->SetDataValues($this->dataSet);
				$this->SetLegend($this->labelSet);
				$this->SetPlotType('pie');
				$data_colors = array('red', 'green', 'blue', 'yellow', 'cyan','magenta', 'brown', 'lavender', 'pink','gray', 'orange');
				if(count($data_colors))
					$this->SetDataColors($data_colors);
				
				$this->SetDataType('text-data-single');
				$this->SetTitle($title);
				
				$this->output_chart();
			}
			
			function output_chart(){
				header('Content-Type: image/png');
				if(is_array($this->dataSet) && count($this->dataSet))
					$this->DrawGraph();
				else
					$this->empty_output();
			}
			
			function empty_output(){
				//we do not have anything to display so
				$limitW = 300;
				$limitH = 300;
				
				$thumb= imagecreatetruecolor ($limitW, $limitH);
		        $bgc = imagecolorallocate ($thumb, 255, 255, 255);
		        $tc = imagecolorallocate ($thumb, 0, 0, 0);
		
		        imagefilledrectangle ($thumb, 0, 0, $limitW, $limitH, $bgc);
		
		        /* Output an error message */
		        imagestring ($thumb, 3, 80, 50, 'NO DATA TO REPORT', $tc);
				
				header('Content-Type: image/png');
				imagepng($thumb);
				imagedestroy($thumb);
				imagedestroy($thumb);
			}
	}

?>