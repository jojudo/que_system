<?php
class file_upload{
	
	var $fileSet;
	var $img_dir;
	var $public_file;
	var $file_limit;
	var $id;
	var $db;
	
	function file_upload($fileset,$db,$folder,$id)
	{
		$this->fileSet=$fileset;
		$this->file_limit = 50000000;
		$this->db=&$db;
		$this->id = $id;
		$this->img_dir=$folder;
	}
	
	function uploadNow($name)
	{
		$file_arr = $this->fileSet;
		$directory = $this->img_dir;
		$returnVal=1;
		
		$explode = explode(".",$file_arr[$name]['name']);
		$ext = strtolower($explode[count($explode)-1]);
		if($this->allowedExt($ext))
		{
			$file_name = $this->id.date("mdyhis").".".$ext;
			$file_target = $directory.$file_name;
			if(!move_uploaded_file($file_arr[$name]['tmp_name'], $file_target)){
				$returnVal=2;
				echo $file_target;
			}
		}
		else
		{
			$returnVal = 3;
		}
		
		$this->public_file = $file_name;
		return $returnVal;
	}
	
	
	function uploadDoc($name,$folder)
	{
		$file_arr = $this->fileSet;
		
		$directory = "uploads/".$folder."/";
		$returnVal=1;
		
		if(!is_dir($directory))
		{
			mkdir($directory);
		}
			
		$explode = explode(".",$file_arr[$name]['name']);
		$ext = strtolower($explode[count($explode)-1]);
		
		if($this->allowedDocs($ext))
		{
			$file_name = $this->id.date("mdyhis").".".$ext;
			$file_target = $directory.$file_name;
			if(!@move_uploaded_file($file_arr[$name]['tmp_name'], $file_target))
				$returnVal=2;
		}
		else
		{
			$returnVal = 3;
		}
		
		$this->public_file = $file_name;
		return $returnVal;
	}
	
	function allowedDocs($ext)
	{
		$ext = strtolower($ext);
		
		switch ($ext){
			case "jpeg":
				return true;
				break;
			case "jpg":
				return true;
				break;
			case "gif":
				return true;
				break;
			case "jpg":
				return true;
				break;
			case "gif":
				return true;
				break;
			case "png":
				return true;
				break;
			case "doc":
				return true;
				break;
			case "docx":
				return true;
				break;
			case "pdf":
				return true;
				break;
			default:
				return false;
		}
	}
		
	function allowedExt($ext)
	{
		$ext = strtolower($ext);
		
		switch ($ext){
			case "jpeg":
				return true;
				break;
			case "jpg":
				return true;
				break;
			case "gif":
				return true;
				break;
			case "jpg":
				return true;
				break;
			case "gif":
				return true;
				break;
			case "png":
				return true;
				break;
			default:
				return false;
		}
	}
	
}
?>