<?php
	interface insertSet{
		//insert record
		public function add_data();
		
		//select all data for edit
		public function edit_data();
		
		//update data record
		public function update_data();
		
		//delete data
		public function delete_data();
	}
