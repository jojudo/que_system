<?php
	class statics{
		 public static function secure_string($val)
	 	{
	 		$var= $val;
	 		$var = trim($var);
	 		$var = strip_tags($var); 		
	 		$var = mysql_escape_string($var);
	 		return $var;
	 	}
	 	
	 	 public static function request($var)
		{	
			$getValue = statics::secure_get($var);
			$postValue = statics::secure_post($var);
			
			$value = $postValue?$postValue:$getValue;
			$value = mysql_escape_string($value);
			return trim($value);
		}
		
		 public static function secure_post($val)
	 	{
	 		$string=isset($_POST[$val])?$_POST[$val]:'';
	 		$var = statics::secure_string($string);
	 		return $var;
	 	}
	 	
	 	 public static function secure_get($val)
	 	{
	 		$string=isset($_GET[$val])?$_GET[$val]:'';
	 		$var = statics::secure_string($string);
	 		return $var;
	 	}
	 	
	 	public static function html_get($val){
	 		$string=$_POST[$val];
	 		$string = mysql_escape_string($string);
	 		
	 		return $string;
	 	}
	 	public static function secure_arrays_get($val){
			return (isset($_GET[$val]))?statics::secure_arrays($_GET[$val]):array();
		}
		
		public static function secure_arrays($arr=array()){
			$newArray = array();
			
			if(is_array($arr)){
				foreach($arr as $index=>$items){
					if(is_array($items)){
						$newArray[$index] = statics::secure_arrays($items);
					}else{
						$newArray[$index] = statics::secure_string($items);
					}
				}
			}
			
			return $newArray;
		}
		
		public static function namesToVar($method){
			$base_arr = (strtolower($method)=="post")?$_POST:$_GET;
			$variables = array();
			foreach($base_arr as $name=>$value){
				$variables[statics::secure_string($name)] = statics::secure_string($value);
			}
			return $variables;
		}
						
	}
	 	
?>