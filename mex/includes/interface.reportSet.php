<?php
	interface reportSet{
		
		//search process
		public function search_data();
		
		//update data reporting
		public function pdf_data();
		
		//csv data report
		public function csv_data();
	}