<?php 
	defined('INSTANCE') or die("No Direct access allowed!");
	
	abstract class helperController{
		function __construct(){
			$this->db = load_class('db');	
			$this->ui = load_class('ui');
			$this->datastore = load_class('datastore');
			$this->profile = $this->datastore->get_profile();
			$this->environment = $this->ui->get_header_type();
			
			//default access to restricted
			$this->access_level = 1;
			
			//any method for retrieving action
			$this->datastore->set_parameter_source("ALL");
			$action = $this->datastore->pick("action");
			$action = $action?$action:"main";
			
			$this->action = $action;
			$this->mod = $this->datastore->pick("mod");
			
			//we bring the default to get for all modules
			$this->datastore->set_parameter_source("GET");
			$this->$action();
		}
		
		abstract protected function main();
				
		function __call($method,$args){
			if($this->environment=="none"){
				$output_arr['msg'] = "The pages you are looking for cannot be found";
				$output_arr['failure'] = 1;
				echo $this->json_output($output_arr);
			}else{
				echo "<p align='Center'>Unknown page <b>$method</b> for module <b>".$this->ui->mod."</b></p>";
			}
		}
		
		function set_mod_access($level){	
			$this->access_level = $level;
		}
		
		function mod_access_level(){
			return $this->access_level;
		}
		
		function mod_restrict_access($maxlevel,$msg='You do not have permission to access this page'){
			if($this->profile['user_level_id']>$maxlevel){
				$this->ui->load_theme_errorfile($msg);
				return TRUE;
			}
			
			return FALSE;
			
		}

		function json_output($json_arr){
			$args['data_arr'] = $json_arr;
			$args['session'] = 1;
			
			$this->ui->load_theme_page_solo("json",$args);
			
			//return ;
		}
		
		function set_mod_javascript($filename){
			$this->ui->set_javascript(MOD_ADDR.$this->ui->mod.US.'js'.US.$filename.'.js');
		}
		
		function set_mod_stylesheet($filename){
			$this->ui->set_stylesheet(MOD_ADDR.$this->ui->mod.US.'css'.US.$filename.'.css');
		}
		
		
			
		
	}
?>