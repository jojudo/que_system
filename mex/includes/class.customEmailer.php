<?php
	class customEmailer extends PHPMailer
	{
		function customEmailer()
		{
			$config = $GLOBALS['config'];
			
			$this->IsSMTP();
			$this->Host = $config->smtp_server; 
			$this->SMTPAuth = true;
			
			$this->Username = $config->smtp_user; 
			$this->Password = $config->smtp_pass; 
			$this->From = $config->smtp_user;
			$this->FromName = $config->site_title." Administrator";
			$this->AddReplyTo($config->smtp_user);
			$this->IsHTML(true);
		}
		
		function send_email($subj,$body)
		{			
			$this->Subject = $subj;
			$mailBody=$body;
		 	$this->Body = $mailBody;
			$word = $this->Send();
			return $word;
		}
	} 
?>