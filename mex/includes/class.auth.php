<?php
	defined('INSTANCE') or die("No Direct access allowed!");
	
	class auth{
		protected $profile, $db ;
		
		function __construct(){
			$this->db = load_class('db');
			$this->datastore = load_class('datastore');
			$this->ui = load_class('ui');			
		}
		
		function authenticate(){
			if(!isset($_SESSION['authenticated'])){
				//this is an entirely new session
				$username = statics::secure_post("username");
				$password = statics::secure_post("password");
				
				if(!$username || !$password){
					$this->load_login_form("Please enter username and password!");
				}
				//getting here means variables have contents
				
				//check records in database
				$result = $this->db->selectQueryFirst("users","user_id,user_level_id,fullname,username,password,strikes,last_action,user_status","username='$username'");
				if($result['user_id']){
					//first thing chek if user status is active
					if(!$result['user_status']){
						$this->load_login_form("Your account is not activated, please contact an admin!");
					}
					//lets see if this user has struck out
					$this->verify_strike_status($result['user_id'],$result['strikes'],$result['last_action']);
					
					//record was found, do password hash
					if(!password_verify($password,$result['password'])){
						$this->record_strike($result['user_id'],$result['strikes']);
						$this->load_login_form("wrong username-password combination");
					}
				}else{
					$this->load_login_form("user account not found!");
				}
				
				//if you were able to arrive here it means we have a valid account
				//create a session ID
				$session_id = session_id();
				$ip_address = $_SERVER['REMOTE_ADDR'];
				$_SESSION['authenticated'] = true;
				$_SESSION['last_access'] = date("U");
				$_SESSION['user_id'] = $result['user_id'];
				
				//save profile records into the datastore
				$this->datastore->set_profile(array('user_id'=>$result['user_id'],'fullname'=>$result['fullname'],'username'=>$result['username'],'last_action'=>date("U")));
				
				//update user login time and wipe out strikes
				$this->db->updateQuery("users",array('session_id'=>$session_id,'strikes'=>0,'last_action'=>date("U"),'ip_address'=>$ip_address),"user_id=".$result['user_id']);
				
				//redirect to homepage to prevent repost when refreshing
				if($this->ui->get_header_type()=="none"){
					$data_arr['username'] = $username;
					$data_arr['session'] = 1;
					$json_arr['data_arr'] = $data_arr;
				
					$this->ui->load_theme_page_noheader("json",$json_arr);	
				}else{
					header("location:index.php");
				}
				exit();
						
			}else{
				//determine if user whats to log out
				$this->clean_logout_check();
				
				//session exists so we will use the clues from session id to know who the user is
				$ip_address = $_SERVER['REMOTE_ADDR'];
				$user_id = $_SESSION['user_id'];
				$last_access = $_SESSION['last_access'];
				$session_id = session_id();
				
				//let us verify session timeout before checking the database for records
				if($last_access+$this->datastore->get_config('session_timeout') < date("U") && $user_id){
					//you overstayed you will get a session timeout
					$this->load_login_form("your session has expired!");
				}
				
				//getting here means session is still good
				$result = $this->db->selectQueryFirst("users","user_id,user_level_id,fullname,username","user_id=$user_id");
				if(!$result['user_id']){
					//just an additional precaution
					$this->load_login_form("There was a session token mismatch! Please login again.");
				}
				
				//save records to datastore
				$this->datastore->set_profile($result);
				
				//update contents of database and session
				$this->db->updateQuery("users",array('session_id'=>$session_id,'last_action'=>date("U"),'ip_address'=>$ip_address),"user_id=".$result['user_id']);
				$_SESSION['last_acces'] = date("U");
				
				//when a requesti was made using the login form
				if(statics::secure_post("action")=="login"){
					$data_arr['user_id'] = $_SESSION['user_id'];
					$data_arr['session'] = 1;
					$json_arr['data_arr'] = $data_arr;
					$this->ui->load_theme_page_noheader("json",$json_arr);	
				}
			}
		}
		
		function verify_strike_status($user_id,$strike,$last_action){
			if($strike>=$this->datastore->get_config('strike_limit')){
				//stroke a bad luck
				if($last_action+$this->datastore->get_config('strike_wait')<=date("U")){
					//remove strikes and allow user to proceed\
					$this->db->updateQuery("users",array('last_action'=>date("U"),'strikes'=>0),"user_id=".$result['user_id']);
					return false;
				}else{
					$this->load_login_form("You have used your fail allowance, please try again after "+($this->datastore->get_config('strike_wait')/60)+" minutes");
				}
			}
		}
		function record_strike($user_id,$strike){
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$this->db->updateQuery("users",array('strikes'=>$strike++),"user_id=".$user_id);
			$this->msg = 'You have used '.$strike++.' of '.$this->datastore->get_config('strike_limit');
			//update last action to mark when to start counting from strikeut
			$this->db->updateQuery("users",array('last_action'=>date("U"),'ip_address'=>$ip_address,'strikes'=>$strike++),"user_id=".$user_id);
			
		}
		
		function load_login_form($msg){
			//clearing just to make sure
			$this->clear_session();
			
			if(isset($this->msg))
				$msg.=' '.$this->msg;
			$args['msg'] = $msg;
			
			if($this->ui->get_header_type()=="none"){
				//Json output
				$data_arr['msg'] = $msg;
				$data_arr['session'] = 0;
				$data_arr['failure'] = 1;
				$json_arr['data_arr'] = $data_arr;
				$this->ui->load_theme_page_noheader("json",$json_arr);				
			}else{
				$body = $this->ui->load_theme_page_solo('login',$args);
			}
			//abrupt end
			exit();
		}
		
		function clear_session(){
			session_unset();
			session_regenerate_id(); 
		}
		
		function clean_logout_check(){
			$logout = statics::request("logout");
			if($logout=="logout"){
				$this->clear_session();
				header("location:index.php");
				exit();
			}
		}
	
	
		
	}
?>