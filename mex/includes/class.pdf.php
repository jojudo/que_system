<?php
define('FPDF_FONTPATH',LIBPATH.DS.'fpdf'.DS.'font'.DS);	
load_lib('fpdf','fpdf','FPDF');

class PDF extends FPDF
{
	protected $settings_arr,$pdf_data_arr; //$pdf,$orientation,$unit,$format;
	
	function __construct(){
		$this->settings_arr['pagestartX'] = 13;
		$this->settings_arr['pagestartY'] = 10;
		$this->settings_arr['linestartX'] = 5;
	}
		
	function set_settings($settings=array()){
		if(count($settings)){
			foreach($settings as $name=>$value){
				$this->settings_arr[$name] = $value;
			}
		}else{
			return false;
		}
	}
	
	function get_settings($name){
		if(isset($this->settingas_arr[$name]))
			return $this->settingas_arr[$name];
		else
			return 0;
	}
	

	function initiate_pdf(){
		//start the pdf rendering
		$this->FPDF($this->settings_arr['orientation'], $this->settings_arr['unit'], $this->settings_arr['size']);
		$this->AddPage();
	}
	
	
	function labels($data_arr){
		$this->nextLine();
		foreach($data_arr as $data){
			$this->cell($data['width'],5,$data['text'],1,'C');
		}
		$this->nextLine();
	}
	
	function entire_row($data_arr){
		$cell_height = 5;
		$total_width = 0;
		//input data
		foreach($data_arr as $data){
			$y = $this->column($data['width'],$data['text'],'0',isset($data['align'])?$data['align']:'L');
			$cell_height = $y['height']>$cell_height?$y['height']:$cell_height;
			$total_width+=$data['width'];
		}
		$this->SetY($y['oldY']);
		$this->SetX($this->settings_arr['linestartX']);
		
		//set the borders
		foreach($data_arr as $data){
			$this->Cell($data['width'],$cell_height,' ',isset($data['border'])?$data['border']:1);
		}
		
		$this->nextLine($cell_height);
		
	}
	
	function spacer($cell_height){
		$this->Cell(250,$cell_height,"",'B');
	}
	
	function column($width,$text,$border=0,$align='L'){
		//width/height/txt/border/align/fill/
		$oldY = $this->GetY();
		$oldX = $this->GetX();
		$this->MultiCell($width,5,$text,$border,$align,0);
		$newY = $this->GetY();
		if($newY>$oldY){
			$this->SetY($oldY);
			$this->SetX($oldX+$width);
		}else{
			$this->SetY($this->settings_arr['pagestartY']);
			$this->SetX($this->settings_arr['pagestartX']);
		}
		
		return array('oldY'=>$oldY,'height'=>$newY-$oldY);
	}
	
	function nextLine($ht=5){
		$this->Ln($ht);
		$this->SetX($this->settings_arr['linestartX']);
	}
	function finalize_pdf(){
		$this->Output();
	}
	
	function SetCol($col)
	{
	    //Set position at a given column
	    $this->col=$col;
	    $x=10+$col*65;
	    $this->SetLeftMargin($x);
	    $this->SetX($x);
	}
	
	function line($width=160,$char="-")
	{
		for($i=0;$i<$width;$i++)
		{
			$this->Cell(1,4,"$char",0,0,'L');
		}
	}
	
	function Footer()
	{
	   // Position at 1.5 cm from bottom
	    $this->SetY(-10);
	    //Arial italic 8
		$this->SetFont('Arial','I',8);
	    //Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		$this->AliasNbPages();
	}
	
	//auto printing functions starts here
	//printing starts here
	function IncludeJS($script) {
		$this->javascript=$script;
	}

	function _putjavascript() {
		$this->_newobj();
		$this->n_js=$this->n;
		$this->_out('<<');
		$this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
		$this->_out('>>');
		$this->_out('endobj');
		$this->_newobj();
		$this->_out('<<');
		$this->_out('/S /JavaScript');
		$this->_out('/JS '.$this->_textstring($this->javascript));
		$this->_out('>>');
		$this->_out('endobj');
	}

	function _putresources() {
		parent::_putresources();
		if (!empty($this->javascript)) {
			$this->_putjavascript();
		}
	}

	function _putcatalog() {
		parent::_putcatalog();
		if (!empty($this->javascript)) {
			$this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
		}
	}
	
	//second printer class
	function AutoPrint($dialog=true)
	{
		//Open the print dialog or start printing immediately on the standard printer
		$param=($dialog ? 'true' : 'false');
		$script="print($param);";
		$this->IncludeJS($script);
	}
	
	function AutoPrintToPrinter($server, $printer, $dialog=false)
	{
		//Print on a shared printer (requires at least Acrobat 6)
		$script = "var pp = getPrintParams();";
		if($dialog)
			$script .= "pp.interactive = pp.constants.interactionLevel.full;";
		else
			$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
		$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
		$script .= "print(pp);";
		$this->IncludeJS($script);
	}
}	
?>