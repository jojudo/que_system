<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>QAds Management Page </title>
	<?php echo $headinclude;?>
	<!-- custom headers here  -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo THEME_ADDR;?>css/main.css" />
</head>
<body topmargin="0">
	<div id="container">
		<div id="banner"></div>
		<div id="top-nav"><?php echo $topnav;?></div>
		<div id="inside-content"><?php echo $body;?></div>
		<div id="footer"></div>
	</div>

</body>
</html>