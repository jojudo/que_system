<!DOCTYPE html>
<html>
<head>
	<title>QAds Management</title>
	<?php echo $headinclude;?>
	<!-- custom headers here  -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo THEME_ADDR;?>css/main.css" />
</head>
<body topmargin="0">
	<div id="login-container">
		<div id="banner"></div>
		<div class="red-strip"></div>
		
		<div class="errormsg ui-state-error ui-corner-all">
			<p>
				<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Alert:</strong>
				<?php echo $msg;?>
			</p>
		</div>
		<div id="login-box">
			<form method="post" action="index.php" id="loginForm">
			<h2>Log in</h2>
			<table cellpadding="3" cellspacing="0">
				<tr>
					<td><label for="username">User ID</label></td>
					<td><input type="text" name="username" size="30"></td>
				</tr>
				<tr>
					<td><label for="password">Password</label></td>
					<td><input type="password" name="password" size="30"></td>
				</tr>
				<tr>
					<td><input type="submit" name="btnlogin" value="Login" id="btn_login"></td>
					<td><a href="#" class="button next" onclick="$('#loginForm').submit();return false;">Login</a></td>
				</tr>
			</table>
			</form>
		</div>

</body>
</html>