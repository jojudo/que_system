<?php 
    defined('INSTANCE') or die("No Direct access allowed!");
    
    //Declare the Main Globals
    
    //let us define external libraries path
    define('LIBPATH', BASE.DS.'libraries'.DS);

    //let us define the internal classes path
    define('CLASSPATH', BASE.DS.'includes'.DS);
    
    //let us define the internal classes path
    define('MODPATH', BASE.DS.'modules'.DS);
    
    $server_name = rtrim($_SERVER['SERVER_NAME']);
    
    //full URL Path
    define('FULL_ADDR', "http://".$server_name.$current_folder.US);
    
    //full URL Lib Path
    define('LIB_ADDR', "http://".$server_name.$current_folder.US.'libraries'.US);
    
    
    //full Module URL Path
    define('MOD_ADDR', "http://".$server_name.$current_folder.US.'modules'.US);
    
    
    //load configuration file
if (file_exists(BASE.DS."configuration.php")) {
    include BASE.DS."configuration.php";
    //$config = new MConfig();
} else {
    if (file_exists(BASE.DS."installation".DS."index.php")) {
        header('location:'.BASE.DS.'installation'.DS.'index.php');
        exit();    
    } else {
        exit("Installation not found, System Error");
    }
}

?>
