<?php 
defined('INSTANCE') or die("No Direct access allowed!");
require CLASSPATH.DS.'classloader.php';
require CLASSPATH.DS.'class.statics.php';
require CLASSPATH.DS.'class.ui.php';
require CLASSPATH.DS.'class.helper.php';
require CLASSPATH.DS.'interface.insertSet.php';
require CLASSPATH.DS.'interface.reportSet.php';
if (!function_exists('password_hash')) {
    include CLASSPATH.DS.'password_functions.php';
}        
