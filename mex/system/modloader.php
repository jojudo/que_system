<?php 
    defined('INSTANCE') or die("No Direct access allowed!");
    
class modloader
{
        
    function __construct()
    {
        $this->boot();
    }
        
    private function boot()
    {
        $this->db = load_class('db');
        $this->ui  = load_class('ui');
        $this->auth = load_class('auth');
        $this->datastore = load_class('datastore');
        $this->auth->authenticate();
            
        $mod = $this->datastore->pick("mod");
        $mod = $mod?$mod:"multicashier";
        $this->ui->set_module($mod);
        $this->module_process($mod);            
            
    }
        
    private function setup_ui_variables()
    {
            
        $uiVars['modPath'] = MOD_ADDR.$this->ui->mod.US;
        $uiVars['mod'] = $this->ui->mod;
        $uiVars['theme_add'] = THEME_ADDR;
        $uiVars['fullPath'] = FULL_ADDR;
        $uiVars['environment'] = $this->datastore->get_config('environment');
            
        $this->ui->set_js_variables($uiVars);
    }

    private function module_process($mod)
    {
        $this->setup_ui_variables();
        $this->ui->ui_start();
        if(file_exists(MODPATH.$mod.DS."helper.php")) {
            include MODPATH.$mod.DS."helper.php";
            $module = new moduleHelper;
        }else{
            $this->ui->load_theme_errorfile('Page not found');
        }
        $this->ui->ui_end(true);
    }
        
    private function authenticate()
    {
        $this->auth = load_class('authenticate');
        if(!$auth->authenticated) {
            echo 'oh no there is a priblem';
            exit();
        }
    }

}
