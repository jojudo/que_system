<?php
	class apiHelper extends moduleHelper{
		function __construct(){
			//echo "I inherit and do not run default construct";
			$this->ui = load_class('ui');
			$this->db = load_class('db');
			$this->datastore = load_class('datastore');
			$this->environment = 'API';	
		}
		
		function api_run($action){
			if($this->api_verified($action))
				$this->$action();
		}
		
		protected function api_verified($action){
			if(!isset($this->api_permitted_actions))
				$this->api_permitted_actions = array();
				
			return in_array($action,$this->api_permitted_actions)?TRUE:FALSE;

		}
		
		protected function api_action_permit($action){
			if(!isset($this->api_permitted_actions))
				$this->api_permitted_actions = array();
				
			$this->api_permitted_actions[] = $action;
		}
		
		protected function api_record_permissions(){
			;
		}
	}
?>