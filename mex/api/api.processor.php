<?php
	//set up error environment
	if(defined($config['environment']))
	{
		switch ($config['environment'])
		{
			case 'development':
				error_reporting(1);
			break;
		
			case 'testing':
			case 'production':
				error_reporting(0);
			break;

			default:
				exit('The application environment is not set correctly.');
		}
	}
	
	$datastore = load_class('datastore');
	//add value to config
	$datastore->set_config($config);

	$api = new api();
?>