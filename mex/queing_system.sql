-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2015 at 07:56 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `queing_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `ad_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `position_id` bigint(10) NOT NULL,
  `ad_type_id` bigint(10) NOT NULL,
  `ad_name` text NOT NULL,
  `filename` text NOT NULL,
  `body_text` text NOT NULL,
  `duration` int(10) NOT NULL,
  `isEnabled` int(1) DEFAULT NULL,
  `order` int(3) NOT NULL,
  PRIMARY KEY (`ad_id`),
  KEY `ad_type_id` (`ad_type_id`),
  KEY `position_id` (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `position_id`, `ad_type_id`, `ad_name`, `filename`, `body_text`, `duration`, `isEnabled`, `order`) VALUES
(10, 1, 2, 'Dragon Katol', 'dragon_katol.mp4', '', 30, 1, 5),
(12, 1, 1, 'Image sample', 'pizza_hut.jpg', '', 5, 1, 6),
(13, 1, 1, 'Koala', 'Koala.jpg', '', 10, 1, 9),
(14, 1, 2, 'Bayantel', 'bayantel.mp4', '', 0, 1, 7),
(15, 1, 1, '', 'Chrysanthemum.jpg', '', 15, 1, 8),
(18, 2, 3, 'Marquee Text 12/22/2014', '', 'We are the champions my friend', 0, 1, 0),
(19, 2, 3, 'Marquee Text 12/22/2014', '', 'Dunkin Donuts  Pasalubong ng Bayan', 0, 1, 0),
(21, 2, 3, 'Marquee Text 12/23/2014', '', 'I was a child roaming the streets of the earth and you are 25 minutes too late', 0, 1, 0),
(22, 2, 3, 'Marquee Text 12/23/2014', '', 'Listen little child, there will be a day when we will be able .. able to say', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ad_positions`
--

CREATE TABLE IF NOT EXISTS `ad_positions` (
  `position_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(10) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ad_positions`
--

INSERT INTO `ad_positions` (`position_id`, `position_name`, `shortname`) VALUES
(1, 'Main ad', 'mainad'),
(2, 'Marquee', 'marquee');

-- --------------------------------------------------------

--
-- Table structure for table `ad_type`
--

CREATE TABLE IF NOT EXISTS `ad_type` (
  `ad_type_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) NOT NULL,
  `extensions` text NOT NULL,
  PRIMARY KEY (`ad_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ad_type`
--

INSERT INTO `ad_type` (`ad_type_id`, `type_name`, `extensions`) VALUES
(1, 'Image', 'jpg,jpeg,png,gif,bmp'),
(2, 'Movie Clip', 'flv,mov,fla,avi,mp4,mkv'),
(3, 'Text', '');

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE IF NOT EXISTS `counters` (
  `counter_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `counter_name` varchar(50) NOT NULL,
  `code` char(2) NOT NULL,
  `startnum` int(3) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`counter_id`, `counter_name`, `code`, `startnum`, `description`) VALUES
(1, 'Payment', 'N', 101, 'Teller 1'),
(2, 'Complants', 'B', 201, '');

-- --------------------------------------------------------

--
-- Table structure for table `counter_session`
--

CREATE TABLE IF NOT EXISTS `counter_session` (
  `counter_session_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `counter_id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `current` int(3) NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `locked` int(1) NOT NULL,
  PRIMARY KEY (`counter_session_id`),
  KEY `counter_id` (`counter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `counter_session`
--

INSERT INTO `counter_session` (`counter_session_id`, `counter_id`, `user_id`, `current`, `starttime`, `endtime`, `locked`) VALUES
(1, 1, 1, 0, '2015-01-25 02:21:37', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `human_line`
--

CREATE TABLE IF NOT EXISTS `human_line` (
  `rowid` bigint(10) NOT NULL AUTO_INCREMENT,
  `counter_id` bigint(10) NOT NULL,
  `que_number` varchar(5) NOT NULL,
  `priority` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  `comments` text,
  `date_inserted` datetime NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `ad_id` (`counter_id`),
  KEY `position_id` (`que_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mods`
--

CREATE TABLE IF NOT EXISTS `mods` (
  `mod_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(50) NOT NULL,
  `parameters` text NOT NULL,
  `date_inserted` datetime NOT NULL,
  PRIMARY KEY (`mod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_access`
--

CREATE TABLE IF NOT EXISTS `mod_access` (
  `mod_access_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `mod_id` bigint(10) NOT NULL,
  `user_level_id` bigint(10) NOT NULL,
  PRIMARY KEY (`mod_access_id`),
  KEY `mod_id` (`mod_id`,`user_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `service_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `service_code` varchar(3) NOT NULL,
  PRIMARY KEY (`service_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `description`, `service_code`) VALUES
(1, 'Customer Support', 'Customer Inquiries', 'C'),
(2, 'New Accounts', 'Marketing', 'A'),
(3, 'Cashier 1', 'Payments', 'P'),
(4, 'Manager', 'Elevated Cases', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `user_level_id` bigint(2) NOT NULL,
  `user_status` int(1) unsigned NOT NULL DEFAULT '0',
  `date_created` int(10) NOT NULL,
  `last_action` bigint(10) NOT NULL,
  `strikes` int(3) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fullname`, `username`, `password`, `session_id`, `ip_address`, `user_level_id`, `user_status`, `date_created`, `last_action`, `strikes`) VALUES
(1, 'Joey Arnold Toga', 'admin', '$2y$10$4VSjyTRoAY0Fllq//LYD3OZ3FfAqHTV5JtwtQAlfCGQd.nn2.p/T2', 'dcmljtpdkqvj0mq481ieb3ias0', '::1', 1, 1, 1396717381, 1422167358, 0),
(2, 'Dannah Marie Achurra', 'dachurra', '$2y$10$AWSLBPOgU9ELvbMrMi8ejOTIK.Octr4Et6WaCkm.FxCcMDn0NWFym', '', '', 1, 1, 1396718845, 1396718845, 0),
(42, 'Jein Pabalinas', 'killjein', '$2y$10$XvCfz5Co04dKpULbZc2t7uKpd/0vWy4syRBPtodsj2hMAF/5CUyvu', '', '', 1, 1, 1418112280, 1418112280, 0),
(45, 'Abdul Jack Hul', 'abdul', '$2y$10$xwmGPEnm3IPhhKkwiPBb4.DdxosKrySOME22o.BRgbXEWEff99hdm', '', '', 2, 1, 1418114219, 1418114219, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_counter`
--

CREATE TABLE IF NOT EXISTS `user_counter` (
  `user_counter_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `counter_id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  PRIMARY KEY (`user_counter_id`),
  KEY `counter_id` (`counter_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE IF NOT EXISTS `user_levels` (
  `user_level_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(20) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`user_level_id`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Cashier');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
