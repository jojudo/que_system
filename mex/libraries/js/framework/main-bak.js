
//jquery manipulation ---- new technologies start here
function createInfo(divID,content,mtitle,ht,wd){
	if(!$("#"+divID).length){
		$("#admin-body").append("<div id='"+divID+"' class='modals'></div>");
		$("#"+divID).dialog({
			bgiframe: true,
			height: ht,
			width: wd,
			modal: true,
			title:mtitle
		});
		$("#"+divID).html(content);
	}else{
		$("#"+divID).dialog('open');	
		$("#"+divID).dialog('option','height',ht);
		$("#"+divID).dialog('option','width',wd);
		$("#"+divID).dialog('option','title',mtitle);
		$("#"+divID).dialog('option','position','middle');
		if(content)
			$("#"+divID).html(content);
	}
}


function loadingInfo(divID,content,mtitle,ht,wd){
	createInfo(divID,content,mtitle,ht,wd);
	$("#"+divID).parent().children(".ui-dialog-titlebar").css("display","none");
	$("#"+divID).dialog({ closeOnEscape: false });
}

function autoloadinginfo(textMessage){
	if(textMessage==null)
		textMessage = "Loading...";
	loadingInfo('windowloader','<div class="loader">'+textMessage+'<br /><img src="images/bar-loader.gif"> <div>','',100,450);
}

function closeautoloadinginfo(){
	$("#windowloader").dialog('close');
}

function dialog_close_button(ID,msg){
	var str = msg+'<br />';
	str+='<input type="button" value="CLOSE" onclick="dialog_closer(\''+ID+'\');">';
	$("#"+ID).html('<p class="loader">'+str+'</p>');
}

function dialog_closer(ID){
	$("#"+ID).dialog('close');
}

//form checking ... enter arrays of ids to check
function checkForm(form_arr) 
{
	var cont=true;
	for (var counter=0; counter<form_arr.length; counter++)
	{
		var objText=form_arr[counter];
		var obj = $("#"+objText);
		
		if(obj.val()=='' || obj.val()==0)
		{
			obj.css("background","#FFFF66");
			obj.focus();
			cont=false;
		}
		else
		{
			obj.css("background","");
		}
				
	}
	
   	if(!cont)
   		alert("Some required fields are missing!");
   		
	return cont;
}

function checkFormOld(form_arr) 
{
	var cont=true;
	for (var counter=0; counter<form_arr.length; counter++)
	{
		objText=form_arr[counter];
		if(document.getElementById(objText)!=null)
		{
			objs = document.getElementById(objText);
			if(objs.type=="select-one")
			{
				eVal = objs[objs.selectedIndex].value
			}
			else
			{
				eVal = objs.value;
			}
				
			if(eVal.length==0)
			{
				objs.style.background="#FFFF66";
				objs.focus();
				cont=false;
			}
			else
			{
				objs.style.background="";
			}
		}		
	}
	
   	if(!cont)
   		alert("Some required fields are missing!");
   		
	return cont;
}

//session failure
function sess_failure(){
	$(".modals").dialog("close");
	var html = '<div class="loader"><img src="images/loader.gif"> Loading Content...</div>';
	createInfo('expiryForm',html,'Session Expired',200,400);
	
	$.ajax({
	  type: "POST",
	  url: "api/user-api.php",
	  async: false,
	  data: "action=expired",
	   success: function(data){
			$("#expiryForm").html(data);
			$("#loginForm").submit(function(){expireLogin();return false;});
	   },
	   error:function(a,b,c){alert('problem loading');}
	 })
}

function expireLogin(){
	var dataSet = $("#loginForm").serialize();
	$("#loginForm :input").attr("disabled","disabled");
	$.ajax({
	  type: "POST",
	  url: "api/user-api.php",
	  async: false,
	  data: "action=login&"+dataSet,
	  success: function(data){
			if(data==0){
				alert('Username and Password combination not found');
				$("#loginForm :input [type==text]").val('');
				$("#loginForm :input").attr("disabled","");
			}else if(data==1){
				$("#expiryForm").dialog('close');
			}else{
				$("#loginForm :input  [type==text]").val('');
				$("#loginForm :input").attr("disabled","");
			}
			
	   }
	 })
}

//bottom bar management
function startBottomBar(){
	var menuRoot = $(".bottom-menu-root");
	var menu = $(".bottom-menu");

	// Hook up menu root click event.
	$("#site-bottom-bar").css("display","block");
	menuRoot
		.attr( "href", "javascript:void( 0 )" )
		.click(
			function(){
				// Toggle the menu display.
				menu.toggle();
				
				// Blur the link to remove focus.
				menuRoot.blur();

				// Cancel event (and its bubbling).
				return( false );
			}
		);
		
	$( document ).click(
		function( event ){
			// Check to see if this came from the menu.
			if (
				menu.is( ":visible" ) &&
				!$( event.target ).closest( "#bottom-menu" ).size()
				){

				// The click came outside the menu, so
				// close the menu.
				menu.hide();

			}
		}
	);
}

//xml managementfunction xmlProcess(data){  
function processXML(data,tag,attrs){
  var val = new array();
  var ctr=0;
  
  //find every Tutorial and print the author
  $(data).find(tag).each(function()
  {
    val[ctr] = $(this).attr(attrs);
	ctr ++;
  });

  return val
 
}  

function hideUnhide(divID)
{
	var objs = $("#"+divID);
	if(objs.css('display')=="none")
		//objs.css('display','block');
		objs.show("blind");
	else
		//objs.css('display','none');
		objs.hide("blind");
		
}

function hideOther(div1,div2)
{
	document.getElementById(div1).style.display="block";
	document.getElementById(div2).style.display="none";
}

function hideMe(div)
{
	$("#"+div).css("display","none");
}

function showMe(div)
{
	$("#"+div).css("display","block");
}

function confirmMe(message,url)
{
 var answer = confirm(message)
 if (answer)
	window.location = url
}

function checkAll(checkbox,check)
{
	
	var leadCheck = document.getElementById(checkbox);	
	var key = 1;
	var chekKey;
	while(document.getElementById(check+key)!=null)
	{
		chekKey = document.getElementById(check+key);
		chekKey.checked=leadCheck.checked;
		key++;
	}
}

function IsNumeric(s){
	return Number(s).toString() != "NaN";
}

function openWindow(url,width,height)
{
	window.open(url,"terms","height="+height+",width="+width+",scrollbars=yes,resize=yes,status=no,toolbars=no");
}


function str_split(str,con)
{
	var strs = str.split(con);
	
	return strs;
}

function addslashes(str) {
	str=str.replace(/\\/g,'\\\\');
	str=str.replace(/\'/g,'\\\'');
	str=str.replace(/\"/g,'\\"');
	str=str.replace(/\0/g,'\\0');
	return str;
}

function checkboxcheckAll(parent,children){
	var val = $("#"+parent).attr("checked");
	
	$("."+children).attr("checked",val);
}

function uncheck(objID){
	$("#"+objID).attr("checked","");
}

function variabletoFunction(varName){
	//only works with global functions
	window[varName]();
}

function returnGlobalVariable(varName){
	//only works with global functions
	return window[varName];
}

function setGlobalVariable(varName,value){
	window[varName] = value;
}