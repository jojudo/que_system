<?php 
	class publicLoader{
		function __construct(){
			$this->boot();
		}
		
		private function boot(){
			//processing resources
			$this->db = load_class('db');
			$this->ui  = load_class('ui');
			$this->datastore = load_class('datastore');
			
			$mod = $this->datastore->pick("mod");
			$mod = $mod?$mod:'multimonitor';
			$this->ui->set_module($mod);
			
			$this->public_run_module();
		}
		
		private function setup_ui_variables(){
			
			$uiVars['modPath'] = PB_ADDR;
			$uiVars['mod'] = $this->ui->mod;
			$uiVars['theme_addr'] = THEME_ADDR;
			$uiVars['fullPath'] = PB_ADDR;
			$uiVars['environment'] = $this->datastore->get_config('environment');
			
			$this->ui->set_js_variables($uiVars);
		}
		
		private function public_run_module(){
			$this->setup_ui_variables();
			$mod = $this->ui->mod;
			if(file_exists(MODPATH.$mod.DS."helper.php")){
				//mod helper to be used as parent
				require MODPATH.$mod.DS."helper.php";
				
				//public helper will be our main class
				require PBPATH.'public.helper.php';
				
				//now that parent and child class is present we instantiate our helper
				$helper = new publicHelper();
				
				//check if we are permitted to go public
				
				//now action starts
				$helper->public_mod_run();
			}else{
				echo 'Page not found';
			}
						
		}
		
		function return_parameters(){
			;
		}
	}
?>