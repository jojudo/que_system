<?php
	//set up error environment
	if(defined($config['environment'])){
		switch ($config['environment']){
			case 'development':
				error_reporting(1);
			break;
			case 'testing':
			case 'production':
				error_reporting(0);
			break;
			default:
				exit('The application environment is not set correctly assigned.');
		}
	}
	
	//this can be made dynamic
	define('THEME',$config['theme']);
	
	//let us define the template base
	define('THEME_BASE',BASE.DS."themes".DS."public".DS.THEME.DS);
	
	//let us define the full URL to template base
	define('THEME_ADDR',FULL_ADDR.US."themes".US."public".US.THEME.US);
	
	
	
	//instantiate datastore for global access
	$datastore = load_class('datastore');
	//add value to config
	$datastore->set_config($config);

	$publicProcess = new publicLoader();
?>