<?php
	session_start();
	define('INSTANCE',1);
	
	chdir('../');
	
	//define environment
	define('ENVIRONMENT', 'development');	
	
	//setup static refences
	define('DS',DIRECTORY_SEPARATOR);
	define('US','/');
	
	//let us define the location of main processors!
	$system_path = realpath('system').DS;
	define('SYSPATH', str_replace("\\", DS, $system_path));
	define('PBPATH', str_replace("\\", DS, realpath('public').DS));
	//let us define the internal classes path
	//define('MODPATH', pb);
	
	//define this file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	
	//define the base of this file
	$base_arr = explode(DS,dirname(__FILE__));
	array_pop($base_arr);
	
	define('BASE',implode(US,$base_arr));
	
	//let us define the Full URL versions of the folders
	$addr_arr = explode(US,$_SERVER['SCRIPT_NAME']);
	array_pop($addr_arr);
	array_pop($addr_arr);
	
	$current_folder = implode(US,$addr_arr);	
	$current_folder = LTRIM($current_folder,DS);
	
	//load the mother of all boards
	
	//basic resources
	require_once SYSPATH.'motherboard.php';
	
	//full public Module URL Path
	define('PB_ADDR',"http://".$server_name.$current_folder.US.'public'.US);
	
	//operational resources
	require SYSPATH.'startupClasses.php';
	
	//public resources
	require PBPATH.'public.loader.php';
	require PBPATH.'public.processor.php';
	
	
?>

