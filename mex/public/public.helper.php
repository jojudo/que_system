<?php
	class publicHelper extends moduleHelper{
		function __construct(){
			//echo "I inherit and do not run default construct";
			$this->ui = load_class('ui');
			$this->db = load_class('db');
			$this->datastore = load_class('datastore');
			$this->environment = 'PUBLIC';	
			
			$action = $this->datastore->pick("action");
			$action = $action?$action:"main";
			
			$this->action = $action;
			
		}
		
		function public_mod_run(){	
			//if($this->mod_is_public())
			if(method_exists($this, "mod_is_public")){
				$action = $this->action;
				$this->ui->ui_start();	
				$this->$action();
				$this->ui->ui_end(TRUE);
			}else{
				echo 'you are not public';
			}
		}
	}
?>