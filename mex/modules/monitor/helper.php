<?php
	class moduleHelper extends helperController{
				
		function __construct(){
			parent::__construct();
		}
		
		function mod_is_public(){
			return TRUE;
		}
		
		protected function main(){
			$custom_settings  = $this->datastore->get_config('monitor');
			
			$args = array();
			$args['videoPlayerWidth'] = $custom_settings['videoPlayerWidth'];
			$args['videoPlayerHeight'] = $custom_settings['videoPlayerHeight'];
			$args['imageWidth'] = $custom_settings['imageWidth'];
			$args['imageHeight'] = $custom_settings['imageHeight'];
			
			$uiVars['mediaPath'] = FULL_ADDR."media".US;		
			$uiVars['buzzerTimerSeconds'] = intval($custom_settings['buzzerTimerSeconds']);
			$uiVars['marqueeTimerSeconds'] = intval($custom_settings['marqueeTimerSeconds']);
			$uiVars['adTimerSeconds'] = intval($custom_settings['adTimerSeconds']);
			
			//set up UI Variables 
			$this->ui->set_js_variables($uiVars);
			
			$this->set_mod_javascript('monitor');	
			$this->set_mod_stylesheet('monitor');	
			$this->ui->use_module_flat('display',$args);
			
		}
		
		function load_big_ads(){
			//we are gonna start the que;
			$args['ads'] = $this->db->selectQueryArr("ads","filename,duration,ad_type_id,ad_name","position_id=1 and isEnabled=1","`order` ASC");
			$args['rows'] = $this->db->num_rows();
			$args['success'] = TRUE;
						
			$this->json_output($args);
		}
		
		function load_marquee_text(){
			//we are gonna start the que;
			$args['ads'] = $this->db->selectQueryArr("ads","body_text","position_id=2 and isEnabled=1","`order` ASC");
			$args['rows'] = $this->db->num_rows();
			$args['success'] = TRUE;
						
			$this->json_output($args);
		}
		
		function check_number(){
			$lastDate = $this->datastore->pick("lastDate");
			$lastDate = strtotime($lastDate);
			
			if(date("U")!=date("U",$lastDate)){
				$buzz  = TRUE;
			}
			
			if(!$lastDate) $buzz = FALSE;
			
			$text = file("media/count.json");
			$json_arr  = json_decode($text[0],TRUE);
			$args['number'] = $json_arr['count'];
			$args['date'] = $json_arr['last_updated'];
			$args['success'] = TRUE;
			$args['buzz'] = $buzz;
			
			$this->json_output($args);
		}
		
		function flag_ready(){
			//we are gonna signal admin that we are ready to accept buzz requests
			$msg ='';
			$json_arr = array('ready'=>1,'last_updated'=>date('Y-m-d H:i:s'));
			$json_text = json_encode($json_arr);
			$fp = fopen("media/ready.json","w+");
			if($fp)	{ $fw = fwrite($fp,$json_text);} else {$msg="unable to create file";}
			fclose($fp);
			
			$args['msg'] = $msg;
			$args['date'] = $json_arr['last_updated'];
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
	}

