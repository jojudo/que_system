<div id="breadcrumb">Admin >> <b>Users</b></div>
<form name="frmOrder" id="frmOrder">
<p id="top-controls">
	<a href="#" id="btnAdd" class="button add">Choose a File</a>
	<!-- <a href="#" id="btnOrder" class="button">Save Order</a> -->
</p>

	<table width="100%" class="data-table" cellspacing="0">
		<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Filename</th>
			<th>duration (seconds)</th>
			<th>Status</th>
			<th>Delete</th>
		</tr>
		<thead>
		<tbody id="sortable">
<?php
if($adverts['rows']){
		$ctr=0;
		foreach($adverts['data'] as $data){ $ctr++;
?>
			<tr id="data-row-<?php echo $data['data_id'];?>" line-id="<?php echo $data['data_id'];?>" >
				<td class="holder" title="drag to Sort">+</td>
				<td align="left"><?php echo $data['type_name'];?> : <span class="ad_name" ><?php echo $data['ad_name'];?></span></td>
				<td><?php echo $data['filename'];?></td>
				<td><?php if($data['type_name']=="Movie") echo '-'; else echo '<class="ad_duration">'.$data['duration'].'</span>';?></td>
				<td><div class="buttongroup"><a href="#" line-id="<?php echo $data['data_id'];?>" class="button-toggle <?php echo $data['isEnabled']?'active':'inactive';?>"><?php echo $data['isEnabled']?'PUBLISHED':'NOT PUBLISHED';?></a></div></td>
				<td>
					<a href="#" line-id="<?php echo $data['data_id'];?>" class="link delete"></a>
					<input type="hidden" value="<?php echo $data['data_id'];?>" name="order[]" />
				</td>
			</tr>
<?php			
		}
}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
		</tfoot>
	</table>

</form>