<?php
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}

		protected function main(){
			if($this->mod_restrict_access(2)) return ;
		
			$adType =2;
			//whe in main go main_ads
			$args['adverts']['data'] = $this->db->selectQueryArr("ads","ad_id data_id,body_text,isEnabled","ad_type_id=3","`order` ASC");
			$args['adverts']['rows'] = $this->db->num_rows();
			
			$this->set_mod_javascript("marquee");
			$this->set_mod_stylesheet("marquee");
			//set libraries to be used
			$this->ui->use_module_flat('ad-list',$args);
			
		}
		
		public function add_data(){
			$this->datastore->set_parameter_source('GET');
			$bodyText = $this->datastore->pick('bodyText');
			
			$continue = true;
			
			//validate password 
			if($bodyText ==''){
				$continue = false;
				$args['message'] = 'Please complete all fields';
			}
		
			if($continue){
				//let us insert the ad now		
				$insertData = array('ad_name'=>'Marquee Text '.date("m/d/Y"),'position_id'=>2,'body_text'=>$bodyText,'duration'=>0,'ad_type_id'=>3,'isEnabled'=>1);
				
				$this->db->insertQuery('ads',$insertData);
				$args['message'] = 'Insert success';
				$insertData['ad_id'] = $this->db->getInsertId();
				$args['insertedData'] = $insertData;
			}
	
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		//user update
		function edit_data(){
			;
		}
		
		function update_data(){
			;
		}
		
		function delete_data(){
			$data_id = $this->datastore->pick('data_id');
			$this->db->deleteQuery("ads","ad_id=$data_id");
			
			$args['success'] = true;
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function resources(){
			$args['data'] = $this->db->selectQueryArr("ad_positions","position_id,position_name");
			$args['success'] = true;
			
			$this->json_output($args);
		}
		
		function activate_ad(){
			$data_id = $this->datastore->pick('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>1),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function deactivate_ad(){
			$data_id = $this->datastore->pick('data_id');('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>0),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function order_data(){
			//
			$this->datastore->set_parameter_source('POST');
			$order= $this->datastore->pick('order');
			$ctr=0;
			foreach($order as $ad_id){ $ctr++;
				$this->db->updateQuery("ads",array("`order`"=>$ctr),"ad_id=$ad_id");
			}
			
			$args['success'] = true;
			$this->json_output($args);
		}
		
	}
