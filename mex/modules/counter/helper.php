<?php
	class moduleHelper extends helperController{
		var $maxCount = 99;
		
		function __construct(){
			parent::__construct();
		}

		protected function main(){
			//get the counters
			//let us check if you have a current session
			
			$this->__get_session();
			$this->set_mod_stylesheet('counter');
			$args['counters'] = $this->__get_counters();
			$this->ui->use_module_flat('table-option',$args);
		}
		
		private function __get_session(){
			$session_arr = $this->db->selectQueryArr('counter_session','counter_id,starttime','user_id='.$this->profile['user_id'],'starttime DESC','0,1');
			//print "you have an existing session";
			if(count($session_arr)){
				header("location:".$this->ui->link_name("multicashier"));
				exit();
			}
		}
		
		function sit(){
			//the cashier has started sitting, record the action until cashier leaves the seat
			$counterid = $this->datastore->pick('var1');
			$fieldval = array('counter_id'=>$counterid,'user_id'=>$this->profile['user_id'],'current'=>0,'starttime'=>date("Y-m-d H:i:s"),'locked'=>1); 
			if($this->db->insertQuery('counter_session',$fieldval))
				;//Now you are seated we will move you to another module
			else
				echo "unable to sit";
				
			return ;
		}
		
		private function __get_counters(){
			return $this->db->selectQueryArr('counters','*');	
		}
		
	}

	
	
	
	
	