<?php
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}

		protected function main(){
			if($this->mod_restrict_access(2)) return ;
			
			$args['users']['data'] = $this->db->selectQueryArr('users S inner join user_levels l using (user_level_id)',"user_id,fullname,level_name,username,user_status,FROM_UNIXTIME(date_created,'%M %d, %Y') as created,S.user_level_id","user_id!=".$this->profile['user_id']." and user_level_id >= ".$this->profile['user_level_id']);
				$args['users']['rows'] = $this->db->num_rows();
				$this->set_mod_javascript('users');
				$this->ui->use_module_flat('user-list',$args);
		}
		
		function add_data(){
			$this->datastore->set_parameter_source("POST");
			$name = $this->datastore->pick('name');
			$username = $this->datastore->pick('username');
			$password = $this->datastore->pick('password');
			$re_password = $this->datastore->pick('re_password');
			$user_position = $this->datastore->pick('position');
			//we will hard code position as it is not required
			$user_level = $user_position?2:3;
			$continue = true;
			
			//validate password 
			if($name =='' || $username=='' || $password == '' ){
				$continue = false;
				$args['message'] = 'Please complete all fields';
			}
			
			if($continue && $password!=$re_password){
				$continue=false;
				$args['message'] = 'Please re-type your password correctly!';
			}
			
			$options = array("cost" => 10);		
			$hash = password_hash($password, PASSWORD_BCRYPT, $options);
			//let us make sure that we were able to hash correctly
			if(!$hash){
				$continue=false;
				$args['message'] = 'Password hashing failed, please contact admin!';
			}
			
			//check if username exists
			$check_arr = $this->db->selectQueryFirst('users','user_id',"username='$username'");	
			if($check_arr['user_id']){
				$continue=false;
				$args['message'] = 'Username is already taken, please choose another one!';
				$args['highlight'] = 'user_username';
			}
			
			if($continue){
				//real saving begins here		
				$this->db->insertQuery('users',array('fullname'=>$name,'username'=>$username,'password'=>$hash,'date_created'=>date("U"),'user_level_id'=>$user_level,'last_action'=>date("U")));
				$args['message'] = 'User insert success';
				$user_id = $this->db->getInsertId();
				$args['insertedData'] = $this->db->selectQueryFirst("users inner join user_levels using (user_level_id)","user_id,fullname,username,user_status,level_name,FROM_UNIXTIME(date_created,'%M %d, %Y') as created","user_id=".$user_id);	
			}
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		//user update
		function edit_data(){
			$user_id = $this->datastore->pick('user_id');
			$args['data'] = $this->db->selectQueryFirst("users","user_id,fullname,username,user_level_id","user_id=".$user_id);	
			
			$args['success'] = true;
			$this->json_output($args);
		}
		
		function update_data(){
			$user_id = $this->datastore->pick('edit_user_id');
			$name = $this->datastore->pick('name');
			$password = $this->datastore->pick('password');
			$re_password = $this->datastore->pick('re_password');
			$user_position = $this->datastore->pick('position');
			$user_level = $user_position?2:3;
			
			
			$continue = true;
			if($this->is_super_user($user_id)){
				$continue = false;
				$args['message'] = 'You can\'t touch this!';
			}
				
			$fields = array("fullname"=>$name,"user_level_id"=>$user_level);
			$args['state'] = 1;
			
				
			//validate if password was given
			if($continue  && $password!=''){
				if($password!=$re_password){
					$continue=false;
					$args['message'] = 'Please re-type your password correctly!';
				}else{
					//create new password
					$options = array("cost" => 10);		
					$hash = password_hash($password, PASSWORD_BCRYPT, $options);
					//let us make sure that we were able to hash correctly
					if(!$hash){
						$continue=false;
						$args['message'] = 'Password hashing failed, please contact admin!';
					}else{
						$fields['password'] = $hash;
					}
				}
			}
			//update noww
			if($continue){
				$result = $this->db->updateQuery("users",$fields,"user_id=$user_id");
				if($result){
					$continue = true;
					$args['data'] = $this->db->selectQueryFirst("users as u left join user_levels using (user_level_id)","user_id,fullname,username,u.user_level_id,level_name,user_status,FROM_UNIXTIME(date_created,'%M %d, %Y') as created","user_id=".$user_id);	
				}else{
					$continue = true;
					$args['message'] = 'No Changes were made';
					$args['state'] = 0;
				}								
			}
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		function delete_data(){
			$user_id = statics::secure_post('user_id');
			if($this->is_super_user($user_id)){
				$args['success'] = false;
				$args['user_id'] = $user_id;
				$args['message'] = 'You can\'t touch this!';
			}else{
				$this->db->deleteQuery("users","user_id=$user_id");	
				$args['success'] = true;
				$args['user_id'] = $user_id;
			}
			
			$this->json_output($args);
			
		}
		
		function activate_user(){
			$user_id = statics::secure_get('user_id');
			$result = $this->db->updateQuery("users",array("user_status"=>1),"user_id=$user_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['user_id'] = $user_id;
			
			$this->json_output($args);
			
		}	
		
		function deactivate_user(){
			$user_id = statics::secure_get('user_id');
			$result = $this->db->updateQuery("users",array("user_status"=>0),"user_id=$user_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['user_id'] = $user_id;
			
			$this->json_output($args);
			
		}
		
		function is_super_user($user_id){
			$level_check = $this->db->selectQueryFirst("users","user_level_id","user_id=".$user_id);	
			if($level_check['user_level_id']==1) return TRUE; else return FALSE;
		}
	}
