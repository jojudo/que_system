function addForm(){	
	createInfo('addUserDiv','...','Add User',300,400);
	loadJsTemplate('addUserDiv',modPath+'js/templates/user-form.tpl','setupAddForm');
}

function setupAddForm(){
	$("#frmNewUser").submit(function(){submitAddform();return false;});
}

function submitAddform(){
	var form_arr = new Array();
	form_arr[0] = 'user_name';
	form_arr[1] = 'user_username';
	form_arr[2] = 'user_password';
	form_arr[3] = 'user_re_password';
	form_arr[4] = 'user_position';
	
	if(checkForm(form_arr)){
		//passed initial screening lets submit it to the system
		if($("#user_re_password").val()!=$("#user_password").val()){
			$("#user_re_password").css("background","#FFFF66");
			$("#user_password").css("background","#FFFF66");
			alert("Passwords do not match!")
			return ;
		}
		//passed password screening
		var dataSet = $("#frmNewUser").serialize();
		param = "action=add_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'POST','submitUserCallback');
	}
		
}

function submitUserCallback(data){
	if(data.success){
		createAlertAutoClose('New user details recorded successfully!',1000);
		closeInfo('addUserDiv');
		appendData(data.insertedData);
	}else{
		alert(data.message);
		if(data.highlight)
			$("#"+data.highlight).css("background","#FFFF66");
			$("#"+data.highlight).focus();
	}
}

function appendData(data){
	
	var str = '<tr id="user-row-'+data.user_id+'">';
		str+='<td>'+data.fullname+'</td>';
		str+='<td>'+data.username+'</td>';
		str+='<td>'+data.level_name+'</td>';
		str+='<td>'+data.created+'</td>';
		str+='<td>';
		str+='<div class="buttongroup"><a href="#" line-id="'+data.user_id+'" class="button-toggle '+(data.user_status?'active':'inactive')+'">'+(data.user_status?'ACTIVE':'INACTIVE')+'</a></div>';
		str+='</td>';
		str+='<td><a href="#"class="link edit" line-id="'+data.user_id+'" ></a><a href="#" line-id="'+data.user_id+'" class="link delete"></a></td>';
		str+='</tr>';
		
	$(".data-table").find("tbody").append(str);
	rebind();
}

//update user
function editRecord(obj){
	var user_id = obj.attr("line-id");
	param = "action=edit_data&user_id="+user_id;
	//now do an ajax call
	ajaxCallJson(param,'GET','editCallback');
}

function editCallback(data){
	createInfo('editDiv','...','Edit User',300,400);
	loadJsTemplate('editDiv',modPath+'js/templates/user-edit-form.tpl','setupEditForm',data.data);
}

function setupEditForm(data){
	
	$("#edit_user_name").val(data.fullname);
	$("#edit_user_username").html(data.username);
	$("#edit_user_id").val(data.user_id);
	
	$("#edit_user_position").val(data.user_level_id);
	
	$("#frmEditUser").submit(function(){updateUser();return false;})
}

function updateUser(){

	var form_arr = new Array();
	form_arr[0] = 'edit_user_name';
	form_arr[1] = 'edit_user_position';
	
	if(checkForm(form_arr)){
		if($("#edit_user_password").val()!=""){
			if($("#edit_user_password").val()!=$("#edit_user_re_password").val()){
				$("#edit_user_re_password").css("background","#FFFF66");
				$("#edit_user_password").css("background","#FFFF66");
				
				alert("Passwords do not match!")
				return ;
			}
			
			
		}
		
		//passed password screening
		var dataSet = $("#frmEditUser").serialize();
		param = "action=update_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'GET','updateCallback');
	}
}


function updateCallback(data){
	if(data.success){
		createAlertAutoClose('User details updated  successfully!',1000);
		closeInfo('editDiv');
		if(data.state==1){
			updateUserRow(data.data);
		}
	}else{
		alert(data.message);
	}
}

function updateUserRow(data){	
	var obj = $("#user-row-"+data.user_id);
	obj.find("td:nth-child(1)").html(data.fullname);
	obj.find("td:nth-child(3)").html(data.level_name);
	
}

//delete user
function deleteRecord(obj){
	var user_id = obj.attr("line-id");
	$("#user-row-"+user_id).css("background","red");
	if(confirm("Are you sure you wish to remove this user?")){
		param = "action=delete_data&user_id="+user_id;
		ajaxCallJson(param,'POST','deleteCallback');
	}else{
		$("#user-row-"+user_id).css("background","");
	}
}

function deleteCallback(data){
	if(data.success){
		$("#user-row-"+data.user_id).animate({
			opacity: 0,
			height: "toggle"
		  }, 500, function() {
			$(this).remove();
		  });
		
	}else{
		$("#user-row-"+data.user_id).css("background","");
		createAlertAutoClose(data.message,5000)
	}
}

//activat account
function activateAccount(obj){
	var user_id = obj.attr("line-id");
	param = "action=activate_user&user_id="+user_id;
	ajaxCallJson(param,'GET','activateCallback');

}

function activateCallback(data){
	var obj = $("#user-row-"+data.user_id);
	var linkObj = obj.find(".inactive");
	linkObj.removeClass("inactive");
	linkObj.addClass("active");
	linkObj.html("Active");
	rebind();
}


//deactivate account
function deactivateAccount(obj){
	var user_id = obj.attr("line-id");
	param = "action=deactivate_user&user_id="+user_id;
	ajaxCallJson(param,'GET','deactivateCallback');

}

function deactivateCallback(data){
	var obj = $("#user-row-"+data.user_id);
	var linkObj = obj.find(".active");
	linkObj.removeClass("active");
	linkObj.addClass("inactive");
	linkObj.html("Inactive");
	rebind();
}


//deactivate bindings
function bindings(){
	//bind them to bindings
	$(".edit").bind("click",function(){editRecord($(this));return false;});		
	$(".delete").bind("click",function(){deleteRecord($(this));return false;});		
	$(".inactive").bind("click",function(){activateAccount($(this));return false;});		
	$(".active").bind("click",function(){deactivateAccount($(this));return false;});		
}

function unbind(){
	//clear anything connected
	$(".edit").unbind("click");
	$(".delete").unbind("click");
	$(".inactive").unbind("click");
	$(".active").unbind("click");
}

function rebind(){
	unbind();
	bindings();
}
$(function() {
	$("#btnAddUser").bind("click",function(){addForm();return false;});		
	bindings();
});