<html>
<head>
	<meta http-equiv="Cache-Control" content="max-age=1800"/>
</head>
<body>
	<form method="post" name="frmeEditUser" id="frmEditUser">
	<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td>Name</td>
			<td><input type="text" name="name" id="edit_user_name" size="30"></td>
		</tr>
		<tr>
			<td>username</td>
			<td><span id="edit_user_username"></span></td>
		</tr>
		<tr>
			<td>password <span class="form-warning">*</span></td>
			<td><input type="password" name="password" id="edit_user_password" size="30"></td>
		</tr>
		<tr>
			<td>re-type password <span class="form-warning">*</span></td>
			<td><input type="password" name="re_password" id="edit_user_re_password" size="30"></td>
		</tr>
		<tr>
			<td>Is Admin</td>
			<td> 
				<input type="checkbox" name="position" id="edit_user_position" value="1" /> Is Admin
			</td>
		</tr>
	</table>
	<span class="form-warning">*leave empty if no change</span>
	
	<p align="right"><input type="submit" name="btnSubmit" class="buttons" value="Update User"></p>
	<input type="hidden" name="edit_user_id" id="edit_user_id">
	</form>
</body>
</html>