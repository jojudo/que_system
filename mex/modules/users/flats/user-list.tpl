<div id="breadcrumb">Admin >> <b>Users</b></div>
<p id="top-controls"><a href="#" id="btnAddUser" class="button add">Add User</a></p>
<?php 
	if($users['rows']){
?>
	<table width="100%" class="data-table" cellspacing="0">
		<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>username</th>
			<th>User Level</th>
			<th>Member Since</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
		<thead>
		<tbody class="autocount">
<?php
			$ctr=0;
			foreach($users['data'] as $data){ $ctr++;
?>
			<tr id="user-row-<?php echo $data['user_id'];?>">
				<td><?php echo $data['fullname'];?></td>
				<td><?php echo $data['username'];?></td>
				<td><?php echo $data['level_name'];?></td>
				<td><?php echo $data['created'];?></td>
<?php if($data['user_level_id']>1) { ?>
				<td>
						<div class="buttongroup"><a href="#" line-id="<?php echo $data['user_id'];?>" class="button-toggle <?php echo $data['user_status']?'active':'inactive';?>"><?php echo $data['user_status']?'ACTIVE':'INACTIVE';?></a></div>
				</td>
				<td><a href="#"class="link edit" line-id="<?php echo $data['user_id'];?>" ></a><a href="#" line-id="<?php echo $data['user_id'];?>" class="link delete"></a></td>
<?php } else {?>
				<td><b>Super Admin</b></td>
				<td><b>Super Admin</b></td>
<?php } ?>
					


			</tr>
<?php			
			}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7">&nbsp;</td>
			</tr>
		</tfoot>
	</table>
<?php
	}
?>
