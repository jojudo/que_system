<?php
	class moduleHelper extends helperController{
		var $maxCount = 99;
		
		function __construct(){
			define('JSNPATH',BASE.DS.'media'.DS.'counters');
			parent::__construct();

		}

		protected function main(){
			//show homepage template
			$this->_get_current_details();
			$count =  $this->get_current_count();
			$args['profile'] = $this->datastore->get_profile();
			
			$args['count'] = $count>=$this->user_details['startnum']?$count:'';
			
			$args['code'] = $count>=$this->user_details['startnum']?$this->user_details['code']:'';
			$args['counter_id'] = $this->user_details['counter_id'];
			$args['title'] = $this->user_details['counter_name'];
			$args['subtitle'] = $this->user_details['description'];
			$args['cashier'] = $this->user_details['description'];
					
			$this->set_mod_stylesheet('cashier');
			$this->set_mod_javascript('cashier');
			
			$this->ui->use_module_flat('frontline',$args);
		}
		
		
		private function _get_current_details(){
			if(!isset($this->user_details)) $this->user_details = $this->db->selectQueryFirst('counters C inner join counter_session S using (counter_id) ','*','S.user_id='.$this->profile['user_id'],'starttime DESC','0,1');
		}
			
			
		function next_customer(){
			$current = $this->get_current_count();
			$priority = $this->get_current_count(1);	
			$current++; 
			
			if($current>$this->user_details['endnum'])
				$current = $this->user_details['startnum'];
			
		
			$this->_create_counter_file($current,$priority);
			
			$args['count'] = $current;
			$args['success'] = TRUE;
			$args['code'] = $this->user_details['code'];
			
			$this->json_output($args);
		}
		
		function next_priority(){
			$priority = $this->get_current_count(1);
			$current = $this->get_current_count();
			$priority++;
			
			//change maximum to 90
			if($priority>90)
				$priority = 1;
				
			$this->_create_counter_file($current,$priority,1);
			
			$args['count'] = $priority;
			$args['success'] = TRUE;
			$args['code'] = $this->user_details['code'];
			
			$this->json_output($args);
		}
		
		function same_customer(){
			//recreate the json file but set checked  to 0
			$this->_get_current_details();
			$text = file(JSNPATH.DS."count-".$this->user_details['counter_id'].".json");
			$json_arr  = json_decode($text[0],TRUE);
			$json_arr['checked'] = 0;
			$json_text = json_encode($json_arr);
			$fp = fopen(JSNPATH.DS."count-".$this->user_details['counter_id'].".json","w+");
			if($fp)	{ $fw = fwrite($fp,$json_text);} else {echo "unable to create file";}
			fclose($fp);
			
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		function get_current_count($getPriority=0){
			$this->_get_current_details();			
			if(!file_exists(JSNPATH.DS."count-".$this->user_details['counter_id'].".json"))
				$this->_create_counter_file(0);	

	
			$text = file(JSNPATH.DS."count-".$this->user_details['counter_id'].".json");
			$json_arr  = json_decode($text[0],TRUE);
			
			if(!$getPriority) $count = $json_arr['count']; else $count = $json_arr['priority'];
				
			$count_date = date("m/d/Y",strtotime($json_arr['last_updated']));
			
			if($count_date!=date("m/d/Y")){
				//we return to zero if 24 hours have passed
				$count = $this->_create_counter_file(0);
			}
			
			return $count;	
		}
		
		//create the count file for current counter
		function _create_counter_file($count=0,$priority=0,$isPriority=0){
			$this->_get_current_details();
			$profile = $this->datastore->get_profile();
			$checked = 0;
			if(!$count){
				//no count or zero go to minimum
				$count = $this->user_details['startnum']-1;
				$checked = 1;
			}
			
			$json_arr = array('counter'=>$this->user_details['counter_id'],'code'=>$this->user_details['code'],'count'=>$count,'priority'=>$priority,'isPriority'=>$isPriority,'last_updated'=>date('Y-m-d H:i:s'),'username'=>$profile['username'],'checked'=>$checked);
			$json_text = json_encode($json_arr);
			$fp = fopen(JSNPATH.DS."count-".$this->user_details['counter_id'].".json","w+");
			if($fp)	{ $fw = fwrite($fp,$json_text);} else {echo "unable to create file";}
			fclose($fp);
			
			return $count;
		}

		
		function are_we_ready(){
			$args['ready'] = $this->check_if_ready();
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		private function check_if_ready(){
			if(file_exists("media/ready.json")){
				$text = file("media/ready.json");
				$json_arr  = json_decode($text[0],TRUE);
				$count_date = strtotime($json_arr['last_updated']);
				$recheck = 60*60;
				//this cannot communicate to the monitor
				if(date("U")>=($count_date+$recheck))
					return FALSE;
				else
					return TRUE;
			}else{	
				return FALSE;
			}
		}
		
	}

	
	
	
	
	