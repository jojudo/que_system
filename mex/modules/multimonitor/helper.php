<?php
	class moduleHelper extends helperController{
				
		function __construct(){
			parent::__construct();
		}
		
		function mod_is_public(){
			return TRUE;
		}
		
		protected function main(){
			$custom_settings  = $this->datastore->get_config('monitor');
			$args = array();			
			
			$uiVars['mediaPath'] = FULL_ADDR."media".US;		
			$uiVars['buzzerTimerSeconds'] = intval($custom_settings['buzzerTimerSeconds']);
			$uiVars['marqueeTimerSeconds'] = intval($custom_settings['marqueeTimerSeconds']);
			$uiVars['adTimerSeconds'] = intval($custom_settings['adTimerSeconds']);
			
			$args['counters'] = $this->db->selectQueryArr('counters','counter_id,counter_name title,description subtitle');
			
			//set up UI Variables 
			$this->ui->set_js_variables($uiVars);
			
			$this->set_mod_javascript('monitor');	
			$this->set_mod_javascript('jquery.marquee.min');	
			$this->set_mod_stylesheet('monitor');	
			$this->ui->use_module_flat('display',$args);
			
		}
		
		function load_big_ads(){
			//we are gonna start the que;
			$args['ads'] = $this->db->selectQueryArr("ads","filename,duration,ad_type_id,ad_name","position_id=1 and isEnabled=1","`order` ASC");
			$args['rows'] = $this->db->num_rows();
			$args['success'] = TRUE;
						
			$this->json_output($args);
		}
		
		function load_marquee_text(){
			//we are gonna start the que;
			$args['ads'] = $this->db->selectQueryArr("ads","body_text","position_id=2 and isEnabled=1","`order` ASC");
			$args['rows'] = $this->db->num_rows();
			$args['success'] = TRUE;
						
			$this->json_output($args);
		}
		
		function check_number(){

			$folder_files = scandir(BASE.DS.'media'.DS.'counters');
			$skip_files = array(".","..");
			
			foreach($folder_files as $file){
				if(!in_array($file,$skip_files)){
					$line = $this->readandupdate(BASE.DS.'media'.DS.'counters',$file);
					if($line) $args['buzzer'][] = $line;
				}
			}			
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		function readandupdate($folder,$file){
			$fullfile = $folder.DS.$file;
			$text = file($fullfile);
			$json_arr  = json_decode($text[0],TRUE);
			$return_arr = array();
			
			if($json_arr['checked']!=1){
				$return_arr['code'] = $json_arr['code'];
				
				if($json_arr['isPriority']==1) $count=intval($json_arr['priority']) ; else $count = intval($json_arr['count']);
				
				$return_arr['count'] = $count;
				$return_arr['counter'] = $json_arr['counter'];
				$return_arr['username'] = $json_arr['username'];
				
				//now we will update the file with new number and flag to checked
				$json_arr['checked'] = 1;
				$json_text = json_encode($json_arr);
				
				$fp = fopen($fullfile,"w+");
				if(fwrite($fp,$json_text)) {;} else { return FALSE;}
				fclose($fp);
				
				return $return_arr;
				
			}else{
				return false;
			}
			
		}
		
		function flag_ready(){			
			//we are gonna signal admin that we are ready to accept buzz requests
			$msg ='';
			$json_arr = array('ready'=>1,'last_updated'=>date('Y-m-d H:i:s'));
			$json_text = json_encode($json_arr);
			$fp = fopen("media/ready.json","w+");
			if($fp)	{ $fw = fwrite($fp,$json_text);} else {$msg="unable to create file";}
			fclose($fp);
			
			$args['msg'] = $msg;
			$args['date'] = $json_arr['last_updated'];
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
	}

