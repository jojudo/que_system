var sounds, media, media_loop, adSet, adCount, adRows, intervalue, currentNumber, buzzDate, marqueeBody;

//marquee functions
function loadMarqueeText(){
	//now do an ajax call
	ajaxCallJson("action=load_marquee_text&headerType=none",'GET','prepareMarquee','hiddenpreload');
}


function prepareMarquee(data){
	var marqueeText = '';
	$.each(data.ads,function(index,value){
		marqueeText+='<span> * '+value.body_text+'</span>';
	})
			
		
	if(marqueeBody!=marqueeText){
		$(".marquee").fadeOut(1000,function(){
			$(this).show();
			$('.marquee').marquee('destroy')
			$(".marquee").html(marqueeText);
			$('.marquee').marquee({duration: 10000});
		});
		
		
	}
	marqueeBody = marqueeText;
}

//main ad functions
function loadMainAds(){
	//now do an ajax call
	ajaxCallJson("action=load_big_ads&headerType=none",'GET','prepareMainAds','hiddenpreload');
}


function prepareMainAds(data){
	var firstTime = true;
	if(adSet) firstTime = false;
		
	adSet = data.ads;
	adRows = data.rows;
	//only start the loop if it is the first time we ran this
	if(firstTime){
		adCount = 0;
		adLoop();
		media.onended = function() {
			adLoop();
		};
	}
}



function adLoop(){
	clearInterval(intervalue);
	if(adSet[adCount].ad_type_id==1){
		//we are an image ad
		pauseMedia(); //stop any media impending
		$("#image_div").show();$("#video_div").hide();
		$("#image_ads").prop("src",mediaPath+adSet[adCount].filename);
		intervalue = setInterval(function(){adLoop()},(adSet[adCount].duration*1000));
	}else{
		$("#image_div").hide();$("#video_div").show();
		var media_file = mediaPath+adSet[adCount].filename;
		media.src = media_file;
		media.volume = 0.5;
		$("#loop").html("Media number "+adCount+" "+adRows);
		media.play();
	}
	
	adCount++;
	if(adCount>=adRows)
		adCount=0;

}

function pauseMedia(){
	media.pause();
}

//Buzzer functions
function checkIfNewNumber(){
	ajaxCallJson("action=check_number&headerType=none",'GET','multibuzz','hiddenpreload');
}

function buzzOrNot(data){
	if(buzzDate!=data.date){
		if(data.buzz){
			var currentNum= $("#currentNumber span:first-child").html();
			if(currentNum!=data.number){
				$("#currentNumber").prepend('<span>'+data.number+'</span>');
				$("#currentNumber span:nth-child(3)").remove();
			}
			ringBuzzer();
		}
	}
	window.buzzDate = data.date;
	currentNumber = data.number;	
}

function multibuzz(data){
	if(data.buzzer){
		$.each(data.buzzer,function(index,value){
			$("#counterBox-"+value.counter).find("h3").html(value.username);
			$("#counter-"+value.counter).html(value.code+value.count);
			ringBuzzer(value.counter);
		})
	}
}

function ringBuzzer(counter){
	sounds.src = mediaPath+"buzzer.wav";
	media.volume = 0;
	sounds.onended = function() {
		media.volume = 0.5
	};
	sounds.play();
	blinkCurrentNumber(counter,2);
	
}

function blinkCurrentNumber(counter,repeat){
	for(var x=1;x<=repeat;x++){
		$("#counter-"+counter).fadeOut(300)
		$("#counter-"+counter).fadeIn(300)
	}
}

function hiddenpreload(){
	;//$("#counters").html("preloading...");
}

function hiddenpostload(){
	;//$("#counters").html("preloading...");
}

function fullscreen(){
	var element = document.documentElement;
	 
	if(element.requestFullscreen) {
		element.requestFullscreen();
	} else if(element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if(element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	} else if(element.msRequestFullscreen) {
		element.msRequestFullscreen();
	}
}

function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

function clickfullScreen(){
	$("#big_center").click(function(){
		if(document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) 
			exitFullscreen(); 
		else 
			fullscreen();
	})
}

function flagReady(){
	//we are gonna tell the admin that we are ready to accept buzzer request
	ajaxCallJson("action=flag_ready&headerType=none",'GET','hiddenpostload','hiddenpreload');
};

//timers
function buzzerTimeout(){
	//schedule when to check next customer
	checkIfNewNumber();
	clearInterval(window.buzzInterval);
	window.buzzInterval = setInterval(function(){buzzerTimeout();},(buzzerTimerSeconds*1000))
}

function adTimeout(){
	//we will reload the contents of the ad every time this is run
	loadMainAds();
	clearInterval(window.adInterval);
	window.adInterval = setInterval(function(){adTimeout();},(adTimerSeconds*1000))
	
}

function marqueeTimeout(){
	//we will reload the contents of the ad every time this is run
	console.clear();
	loadMarqueeText();
	clearInterval(window.mqInterval);
	window.mqInterval = setInterval(function(){marqueeTimeout();},(marqueeTimerSeconds*1000))
	
}

function readyTimeout(){
	//we will continually inform the admin that we are ready, do this per 50 mins admin checks every hour
	flagReady();
	clearInterval(window.readyInterval);
	window.readyInterval = setInterval(function(){readyTimeout()},3000000)
	
}

function resource_load(){
	clickfullScreen();	
	
	media = $("#video_ads")[0];
	sounds = $("#buzzer")[0];
	
	adTimeout();
	buzzerTimeout();
	marqueeTimeout();
	readyTimeout();
}


$(function() {
	resource_load();
});