<?php
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}

		protected function main(){
			if($this->mod_restrict_access(2)) return ;
			
			
			
			$table = "counters C";
			$fields = "counter_id as data_id,counter_name,description,code,startnum,endnum";
			
			$counters = $this->db->selectQueryArr("counters",$fields);
			$args['users']['rows'] = $this->db->num_rows();
			
			$ctr=0;
			
			foreach($counters as $counter){
				$table = "users u inner join counter_session c using (user_id)"; 
				$users = $this->db->selectQueryArr($table,"u.user_id,u.username","c.counter_id = ".$counter['data_id']);
				if(count($users))
					$counters[$ctr]['users'] = $users;
				else
					$counters[$ctr]['users'] = array();
				$ctr++;
			}
			
			$args['users']['data'] = $counters;
			
			$this->set_mod_javascript('counters');
			$this->set_mod_stylesheet('counters');
			
			$this->ui->use_module_flat('data-list',$args);
		}
		
		function add_data(){
			$this->datastore->set_parameter_source("POST");
			$name = $this->datastore->pick('name');
			$startnum = $this->datastore->pick('startnum');
			$endnum = $this->datastore->pick('endnum');
			$code = $this->datastore->pick('code');
			$desc = $this->datastore->pick('desc');
				
			//real saving begins here	
			$fields	= array('counter_name'=>$name,'code'=>$code,'startnum'=>$startnum,'endnum'=>$endnum,'description'=>$desc);
			$this->db->insertQuery('counters',$fields);
			$args['message'] = 'Insert success';

			$fields['data_id'] = $this->db->getInsertId();
			$args['insertedData'] = $fields;
			
			
			$args['success'] = TRUE;
			$this->json_output($args);
		}
		
		//user update
		function edit_data(){
			$data_id = $this->datastore->pick('data_id');
			$args['data'] = $this->db->selectQueryFirst("counters","*","counter_id=".$data_id);	
			
			$args['success'] = true;
			$this->json_output($args);
		}
		
		function update_data(){
			$data_id = $this->datastore->pick('edit_data_id');
			$name = $this->datastore->pick('name');
			$startnum = $this->datastore->pick('startnum');
			$endnum = $this->datastore->pick('endnum');
			$code = $this->datastore->pick('code');
			$desc = $this->datastore->pick('desc');
			
			$fields = array("counter_name"=>$name,"code"=>$code,"startnum"=>$startnum,"endnum"=>$endnum,"description"=>$desc);
			$args['state'] = 1;

			$result = $this->db->updateQuery("counters",$fields,"counter_id=$data_id");
			if($result){
				$continue = true;
				$args['data'] = $fields;
				$args['data']['data_id'] = $data_id;
			}else{
				$continue = true;
				$args['message'] = 'No Changes were made';
				$args['state'] = 0;
			}								
			
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		function delete_data(){
			$data_id = statics::secure_post('data_id');
			
			$this->db->deleteQuery("counters","counter_id=$data_id");	
			$args['success'] = true;
			$args['data_id'] = $data_id;
			
			
			$this->json_output($args);
			
		}
		
		function users(){
			$data_id = $this->datastore->pick('data_id');
			$args['users'] = $this->db->selectQueryArr("users","user_id,username","user_level_id!=1");	
			
			$args['success'] = true;
			$args['data_id'] = $data_id;
			$this->json_output($args);
		}
		
		function users_assign(){
			$data_id = $this->datastore->pick('data_id');
			$line_id = $this->datastore->pick('line_id');
			$name = $this->datastore->pick('name');
			
			$session_check = $this->db->selectQueryFirst("counter_session","counter_session_id","counter_id=$data_id and user_id = $line_id");
			if(!isset($session_check['counter_session_id'])){	
				//delete any other seat user was sitting in
				$this->db->deleteQuery("counter_session","user_id=$line_id");
				
				//insert a new position
				$fields	= array('counter_id'=>$data_id,'user_id'=>$line_id,'starttime'=>date("Y-m-d H:i:s"),'locked'=>1);
				$this->db->insertQuery('counter_session',$fields);
				
				//delete any other user sitting here
				//$this->db->deleteQuery("counter_session","counter_id=$data_id and user_id!=$line_id");
				
				$session_check = $this->db->selectQueryFirst("counter_session","counter_id","counter_id!=$data_id and user_id=$line_id");
				if(count($session_check)) $clear_id = $session_check['counter_id'];
				
				
			}				

			$args['success'] = true;
			$args['name'] = $name;
			$args['data_id'] = $data_id;
			$args['line_id'] = $line_id;
			$args['clear_id'] = isset($session_check['counter_id'])?$session_check['counter_id']:0;
			
			$this->json_output($args);
		}
		
	}
