function addForm(){	
	createInfo('addUserDiv','...','Add Counter',300,400);
	loadJsTemplate('addUserDiv',modPath+'js/templates/add-form.tpl','setupAddForm');
}

function setupAddForm(){
	$("#frmNewUser").submit(function(){submitAddform();return false;});
}

function submitAddform(){
	var form_arr = new Array();
	form_arr[0] = 'add_counter_name';
	form_arr[1] = 'add_counter_start';
	form_arr[2] = 'add_counter_end';

	
	if(checkForm(form_arr)){
		//passed password screening
		var dataSet = $("#frmNewUser").serialize();
		param = "action=add_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'POST','submitUserCallback');
	}
		
}

function submitUserCallback(data){
	if(data.success){
		createAlertAutoClose('New user details recorded successfully!',1000);
		closeInfo('addUserDiv');
		appendData(data.insertedData);
	}else{
		alert(data.message);
		if(data.highlight)
			$("#"+data.highlight).css("background","#FFFF66");
			$("#"+data.highlight).focus();
	}
}

function appendData(data){
	
	var str = '<tr id="user-row-'+data.data_id+'">';
		str+='<td>'+data.counter_name+'</td>';
		str+='<td>'+data.code+'</td>';
		str+='<td>'+data.startnum+'</td>';
		str+='<td>'+data.endnum+'</td>';
		str+='<td>'+data.description+'</td>';
		str+='<td><a href="#" class="change-button">Add User</a></td>';
		str+='<td><a href="#"class="link edit" line-id="'+data.data_id+'" ></a><a href="#" line-id="'+data.data_id+'" class="link delete"></a></td>';
		str+='</tr>';
		
	$(".data-table").find("tbody").append(str);
	rebind();
}

//update user
function editRecord(obj){
	var data_id = obj.attr("line-id");
	param = "action=edit_data&data_id="+data_id;
	//now do an ajax call
	ajaxCallJson(param,'GET','editCallback');
}

function editCallback(data){
	createInfo('editDiv','...','Edit User',300,400);
	loadJsTemplate('editDiv',modPath+'js/templates/data-edit-form.tpl','setupEditForm',data.data);
}

function setupEditForm(data){
	
	$("#edit_counter_name").val(data.counter_name);
	$("#edit_counter_code").val(data.code);
	$("#edit_counter_start").val(data.startnum);
	$("#edit_counter_end").val(data.endnum);
	$("#edit_counter_description").val(data.description);
	
	$("#edit_data_id").val(data.counter_id);
	
	$("#frmEditUser").submit(function(){updateData();return false;})
}

function updateData(){

	var form_arr = new Array();
	form_arr[0] = 'edit_counter_name';
	form_arr[1] = 'edit_counter_start';
	form_arr[2] = 'edit_counter_end';
	
	if(checkForm(form_arr)){		
		//passed password screening
		var dataSet = $("#frmEditUser").serialize();
		param = "action=update_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'GET','updateCallback');
	}
}


function updateCallback(data){
	if(data.success){
		createAlertAutoClose('User details updated  successfully!',1000);
		closeInfo('editDiv');
		if(data.state==1){
			updateUserRow(data.data);
		}
	}else{
		alert(data.message);
	}
}

function updateUserRow(data){	
	var obj = $("#user-row-"+data.data_id);
	obj.find("td:nth-child(1)").html(data.counter_name);
	obj.find("td:nth-child(2)").html(data.code);
	obj.find("td:nth-child(3)").html(data.startnum);
	obj.find("td:nth-child(4)").html(data.endnum);
	obj.find("td:nth-child(5)").html(data.description);
	
}

//delete user
function deleteRecord(obj){
	var data_id = obj.attr("line-id");
	$("#user-row-"+data_id).css("background","red");
	if(confirm("Are you sure you wish to remove this user?")){
		param = "action=delete_data&data_id="+data_id;
		ajaxCallJson(param,'POST','deleteCallback');
	}else{
		$("#user-row-"+user_id).css("background","");
	}
}

function deleteCallback(data){
	if(data.success){
		$("#user-row-"+data.data_id).animate({
			opacity: 0,
			height: "toggle"
		  }, 500, function() {
			$(this).remove();
		  });
		
	}else{
		$("#user-row-"+data.data_id).css("background","");
		createAlertAutoClose(data.message,5000)
	}
}


function changeuser(obj){
	var data_id = obj.attr("line-id");
	var param = "action=users&data_id="+data_id;
	ajaxCallJson(param,'GET','usersList');
}

function usersList(data){
	var str = '';
	$.each(data.users,function(index,value){
		str+='<a href="#" line-id="'+value.user_id+'" data-id = "'+data.data_id+'" class="user-assign change-button">'+value.username+'</a> ';
	})
	
	createInfo('usersListDiv',str,'Assign User',200,300);
	$(".user-assign").click(function(){assignNow($(this));return false;});
}


function assignNow(linkObj){
	var data_id = linkObj.attr("data-id");
	var line_id = linkObj.attr("line-id");
	var name = linkObj.html()
	var param = "action=users_assign&data_id="+data_id+"&line_id="+line_id+"&name="+name;
	ajaxCallJson(param,'GET','assignUserPublic');
}

function assignUserPublic(data){
	//remove previous seat
	$("#user-line-"+data.line_id).remove();
	//now add the button
	
	var str = '<a class="change-button" id="user-line-'+data.line_id+'" line-id="'+data.line_id+'" href="#" title="click to remove">'+data.name+'</a>';
	$("#users-box-"+data.data_id).append(str);

	//just clearing
	$("#user-assign").unbind("click");
	closeInfo('usersListDiv');
}

//deactivate bindings
function bindings(){
	//bind them to bindings
	$(".edit").bind("click",function(){editRecord($(this));return false;});		
	$(".delete").bind("click",function(){deleteRecord($(this));return false;});		
	$(".change-button").bind("click",function(){changeuser($(this));return false;});		
}

function unbind(){
	//clear anything connected
	$(".edit").unbind("click");
	$(".delete").unbind("click");
	$(".change-button").unbind("click");
}

function rebind(){
	unbind();
	bindings();
}
$(function() {
	$("#btnAddUser").bind("click",function(){addForm();return false;});		
	bindings();
});