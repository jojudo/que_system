<div id="breadcrumb">Admin >> <b>Counters</b></div>
<p id="top-controls"><a href="#" id="btnAddUser" class="button add">Add Counter</a></p>
<?php 
	if($users['rows']){
?>
	<table width="100%" class="data-table" cellspacing="0">
		<thead>
		<tr>
			<th>#</th>
			<th>Counter Name</th>
			<th>Code</th>
			<th>Start Number</th>
			<th>End Number</th>
			<th>Description</th>
			<th>Username</th>
			<th>Action</th>
		</tr>
		<thead>
		<tbody class="autocount">
<?php
			$ctr=0;
			foreach($users['data'] as $data){ $ctr++;
?>
			<tr id="user-row-<?php echo $data['data_id'];?>">
				<td><?php echo $data['counter_name'];?></td>
				<td><?php echo $data['code'];?></td>
				<td><?php echo $data['startnum'];?></td>
				<td><?php echo $data['endnum'];?></td>
				<td><?php echo $data['description'];?></td>
				<td>
					<div id="users-box-<?php echo $data['data_id'];?>">
					<?php 
						if(count($data['users'])){ 
						foreach($data['users'] as $user){
					?>
						<a class="change-button" id="user-line-<?php echo $user['user_id'];?>" line-id="<?php echo $user['user_id'];?>" href="#" title="click to remove"><?php echo $user['username'];?></a>
					<?php		
						}
					?>
						
					<? } ?>
					</div>
				</td>
				<td>
					<a href="#"class="link edit" line-id="<?php echo $data['data_id'];?>" ></a><a href="#" line-id="<?php echo $data['data_id'];?>" class="link delete"></a>
					<a href="#" class="link add change-button" line-id="<?php echo $data['data_id'];?>" id="change-button-<?php echo $data['data_id'];?>" title="add User"></a>
				</td>	
			</tr>
<?php			
			}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8">&nbsp;</td>
			</tr>
		</tfoot>
	</table>
<?php
	}
?>
